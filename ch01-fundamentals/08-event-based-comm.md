# Event Based Communication

An **Event Bus** handles notifications/events being emitted from a service. An
event is an object that describes something that is happening in the service.

![event bus diagram](./event-bus.png)

**Note:** This method is not often used, as it shares **all the downsides of
sync communication**!
