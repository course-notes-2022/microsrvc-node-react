# Data in Microservices

## What are the Big Challenges With MSs?

1. **Data Management between services**: i.e. the way in which we store data
   **inside** a service, and the way we communcate data **between services**.

### Database-Per-Service Pattern

In a MS, each service gets its own database (if it needs a database). Services
will **never** attempt to access data from another service's database. The
reasons include:

- **We want each service to run independently of other services** (Service A
  should not be dependent on Service B's DB being functional)

- DB schema/structure might change unexpectedly

- Some services might function more efficiently with different types of DBs
  (e.g. SQL vs. NoSQL)
