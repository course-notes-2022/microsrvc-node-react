# Pros and Cons of Async Communication

### Pros

- Service D has **zero dependencies** on other srvices
- Service D will be very fast

### Cons

- Data duplicaton. Paying for extra storage & extra DB
- Harder to understand
