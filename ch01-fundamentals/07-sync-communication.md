# Syncing Communication Between Servers

Two general strategies for communicating between services:

1. **Sync**: Services communicate with each other using **direct** requests:

![sync communication](./07-sync-communication.md)

### Pros

- Easy to understand
- Service D won't need its own database
- Can introduce web of request dependencies

### Cons

- Introduces a dependency upon other services
- If an inter-service request fails, the overall request fails
- The entire request is only as fast as the **slowest** request

2. **Async**: Services communicate with each other using **events**
