# A Crazy Way of Storing Data

Let's define an **exact goal** for Service D, as follows:

**Given the ID of a user, show the title and image for every product they have
ever ordered.**

How can we add a **database** that serves this request? One solution might look
like:

![service d schema](./service-d-schema.png)

We need to have Service A, B, and C **tell** Service D when events in their DBs
that are important to Service D occur. We can do this by having Service A, B,
and C **emit events** whenever a user signs up, a product is purchased, etc, to
the **Event Bus**. The event bus takes that event and **sends it** to Service D.
Service D then records the event information in its own DB as it needs to.
