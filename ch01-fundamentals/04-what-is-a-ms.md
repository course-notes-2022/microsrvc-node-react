# What is a Microservice?

In a **monolithic** server, all the server-side code exists in a single
application that handles all requests coming from the requesting agent. It
contains routing, middleware, business logic and database access to implement
all features of the app.

![monolith architecture](./monolith.png)

A **microservice** contains the routing, middleware, business logic and database
access to implement **one feature** of the app:

![microservice architecture](./microservice.png)

Each service is **entirely self-contained**, including its own database. If any
of the other services fail for any reason, the remaining services **will
continue to work**.
