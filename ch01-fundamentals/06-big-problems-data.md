# Big Problems with Data

Why is data management between services so challenging?

1. How to get information required by a service, that lives in the database of
   another service, **without communicating directly** with the other service's
   database?
