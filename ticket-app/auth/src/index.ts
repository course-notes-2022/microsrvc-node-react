import express from 'express';
import { json } from 'body-parser';
import { currentUserRouter } from './routes/current-user';
import { signinRouter } from './routes/signin';
import { signoutRouter } from './routes/signout';
import { signupRouter } from './routes/signup';

const server = express();

const PORT = 3000;

server.use(json());

server.use(currentUserRouter);
server.use(signinRouter);
server.use(signoutRouter);
server.use(signupRouter);

server.listen(PORT, () => {
  console.log(`SERVER listening on port ${PORT}!!!!!`);
});
