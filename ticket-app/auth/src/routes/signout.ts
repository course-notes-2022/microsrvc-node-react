import express from 'express';

const router = express.Router();

router.post('/api/users/signout', (req, res) => {
  res.status(200).json({ msg: 'Hi there!' });
});

export { router as signoutRouter };
