import express from 'express';

const router = express.Router();

router.get('/api/users/currentuser', (req, res) => {
  res.status(200).json({ msg: 'Hi there!' });
});

export { router as currentUserRouter };
