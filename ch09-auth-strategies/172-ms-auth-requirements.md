# Microservices Auth Requirements

Let's now make a decision on cookies vs. JWT for authentication. We should
review the **requirements of our application** to help us make the right
decision.

## Requirement 1

![requirement 1](./screenshots/requirement-1.png)

Inside the ticket purchase logic, we want to know:

- Is the user logged in?
- Can the user purchase a ticket?

This implies a requirement that:

> The `auth` mechanism needs to **tell us information** about the user

## Requirement 2

![requirement 2](./screenshots/requirement-2.png)

Imagine that we want admins to be able to create a free coupon code. We only
want the ability to create a free coupon code to be restricted to **admin
users**. This implies a requirement that:

> The `auth` mechanism needs to include **authorization info**.

## Requirement 3

![requirement 3](./screenshots/requirement-3.png)

As we discussed earlier, we want the auth mechanism to be able to expire
**securely** after a configurable period of time.

## Requirement 4

We are going to have a client app that will send requests to our different
services. Recall that each service may be written in a different
**language/framework**. This implies that

> The auth mechanism should be easily understoood by many languages.

Also:

> We don't want to require a service to **store data** in order to support
> authorization.

Based on our requirements, we probably want to select **JWT**s for our
authorization.
