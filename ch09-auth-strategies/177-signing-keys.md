# JWT Signing Keys

In the last lesson, we were able to sign up and receive our cookie. The value
**inside** the cookie is not _technically_ our JWT, but a **base64-encoded
version of the token**.

If we **copy the token value** and paste it into `base64decode.org`, it'll
decode our token for us:

```json
{
  "jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY1NDUxYWI1NTUzMTI3ZTljZDAxMWMwNCIsImVtYWlsIjoiYmFyQGJhei5jb20iLCJpYXQiOjE2OTkwMjc2Mzd9.Kvfs4cGF16B0vMJcH0mdpYauk__6ZUVeO7C8zi1J6Co"
}
```

This is our **actual JWT**.

If we take the `jwt` value and got to `jwt.io`, we can decode the values **from
the token**:

![decoded JWT](./screenshots/decoded-jwt.png)

Note that if we **change the value** in the "Encoded" section in **any way**,
we're immediately alerted that the token signature is **not valid**. This is our
indication that the token has been tampered with, and should not be trusted.

Note that if we **remove** the "asdf", **we can still see the information in the
token (username, etc.)**.

> _Anyone can always see the information in a JWT payload_. It is the fact that
> we can _verify that the token has not been tampered with_ that is important.

## The Importance of Signing Keys

When we generated our JWT, we passed a **payload** and a **signing key** to our
JWT generating library to produce the token. Any time we want to receive the
token in **another service** and **validate it**, that service **needs to have
the signing key**.

We need to share the signing key with other services, but we also need to make
sure no one else gets it! Hard-coding it in plain text is not a good idea. How
can we make sure our key is secure, but available to our other services?
