# Issues with JWTs and Server-Side Rendering

![normal react app flow](./screenshots/normal-react-app-flow.png)

With a normal React app, when a user makes a browser request for
`ticketing.dev`, the client app servier first responds with a **bare-bones**
HTML file. Once the browser receives it, it makes a **follow-up** request for
all the **JS** files included in the template's `<script>` tag. The React app
might make some further requests for **data** (e.g. to the `OrdersService`
service).

At what point in time do we need to communicate **authentication info** from the
**browser** to the **backend**? Usually at the **point the request for _data_ is
made**:

![authentication at data request](./screenshots/auth-at-data-request.png)

We'd want to include the JWT with the **3rd request**, in the headers, request
body, or perhaps cookies.

## What About SSR React Apps?

What about a **server-side rendered app**?

![ssr react app](./screenshots/ssr-react-app-flow.png)

Our **client** needs to fetch data from any of the services from which it
requires data. This means we need to communicate the auth token with the **very
first request**. This presents a **big issue**, because when you request a
domain `foo.com` from a browser, the `foo.com` server has no ability to run
Javascript on your computer **before an HTML file is received**!

![giant issue with ssr](./screenshots/issue-w-ssr.png)

This means that:

- We cannot append a JWT to the **headers** of the initial request.

- We cannot send a JWT in the `body` of the initial request.

- Our only option of sharing a token with the backend server **must** be stored
  inside a **cookie**!

We'll use **cookies AND JWT together** for our authentication system.
