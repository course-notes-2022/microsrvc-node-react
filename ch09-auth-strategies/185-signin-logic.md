# Sign In Logic

Let's continue with the signin flow. Let's attempt to find a user with the same
email as the one currently attempting to register. If the user is found, we will
issue a new JWT and pass it back to the client in a cookie.

Add the following to `signin.ts`:

```ts
import express, { Request, Response } from 'express';
import { body } from 'express-validator';
import { validateRequest } from '../middleware/validate-request';
import { User } from '../models/user';
import { BadRequestError } from '../errors/bad-request-error';
import { Password } from '../services/password';
import jwt from 'jsonwebtoken';

const router = express.Router();

router.post(
  '/api/users/signin',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .notEmpty()
      .withMessage('You must supply a password'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { email, password } = req.body;
    const existingUser = await User.findOne({ email });
    if (!existingUser) {
      throw new BadRequestError('Invalid credentials');
    }

    const passwordsMatch = await Password.compare(
      existingUser.password,
      password
    );
    if (!passwordsMatch) {
      throw new BadRequestError('Invalid credentials');
    }

    // Generate JWT
    const userJwt = jwt.sign(
      {
        id: existingUser.id,
        email: existingUser.email,
      },
      process.env.JWT_KEY! // Tells Typescript that we know JWT_KEY is defined
    );

    // Store it on session object
    // Create a new object to workaround
    // Typescript definitions
    req.session = {
      jwt: userJwt,
    };

    return res.status(200).json(existingUser);
  }
);

export { router as signinRouter };
```
