# Huge Issues with Authentication Strategies

Imagine user `ABC` comes to our app to sign in through our `auth` service. `ABC`
supplies a correct email/password, and the auth service issues an ID token back
to ABC.

If we follow option #2, ABC can use the token to buy a ticket or whatever else.

Further imagine that we want to **ban** ABC from using our services. An admin
user submits a request to auth service to ban the user `ABC`. Auth updates the
DB to reflect that ABC has been banned.

In theory, **one** of our services says that ABC is banned.

Imagine that ABC makes another request to do something in our app. **If ABC
still has the valid token**, ABC can still access orders service, for example,
as we **do not have any checks in order service to ensure that ABC is allowed
in**:

![abc is not banned after all](./screenshots/abc-is-not-banned-after-all.png)

At no point in option 2 do we reach out to auth service to ensure that ABC is
still authenticated and authorized.

This is the fundamental problem with option 2: **we can update data in one
location about a user's status, but the _other services_ don't know about it**!
