# Formatting JSON Properties

Let's remove the `password` and `__v` attributes and remap the `_id` property to
something more standard, like `id` in our response body.

As it turns out, we can **override** the default way that JS turns an object
into JSON by adding a `toJSON()` method on the object. JS will call this method
when invoking the `JSON.stringify()` method on the object.

Mongoose has a `toJSON` property that achieves similar results. One way to use
it would be as follows. Make the following changes to `user.ts`:

```ts
import mongoose from 'mongoose';
import { Password } from '../services/password';

interface UserAttrs {
  email: string;
  password: string;
}

interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAttrs): UserDoc;
}

interface UserDoc extends mongoose.Document {
  email: string;
  password: string;
}

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  // Add a second property to the Schema definition
  // an object with a toJSON property as follows
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.password;
        delete ret['__v'];
      },
    },
  }
);

userSchema.statics.build = (attrs: UserAttrs) => {
  return new User(attrs);
};

userSchema.pre('save', async function (done) {
  if (this.isModified('password')) {
    const hashed = await Password.toHash(this.get('password'));
    this.set('password', hashed);
  }
  done();
});

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

const user = User.build({
  email: 'test@test.com',
  password: 'secret',
});

export { User };
```

Send another request from Postman, and verify the desired properties are present
in the response:

![response body transformation](./screenshots/response-body-transformation.png)
