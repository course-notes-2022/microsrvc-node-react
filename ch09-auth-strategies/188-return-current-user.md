# Returning the Current User

Let's implement the route handler for obtaining the current user. Refactor
`current-user.ts` as follows:

```ts
import express from 'express';
import jwt from 'jsonwebtoken';

const router = express.Router();

router.get('/api/users/currentuser', (req, res) => {
  // Inspect req.session.jwt
  if (!req.session || !req.session.jwt) {
    return res.send({ currentUser: null });
  }

  try {
    // If the token has been tampered with, verify()
    // will throw an error
    const payload = jwt.verify(req.session.jwt, process.env.JWT_KEY!);
    res.send({ currentUser: payload });
  } catch (err) {
    res.json({ currentUser: null });
  }
});

export { router as currentUserRouter };
```

**Ensure that you have a signed-up and signed-in user created** before
completing the following steps.

In Postman, send a `GET` request to
`https://ticketing.dev/api/users/currentuser`. Set the `Content-Type` header to
`application/json`. Note that Postman will automatically send the
`ticketing.dev` `session` cookie to any requests to the same
`https://ticketing.dev` domain. You should receive a response body similar to
the following:

```json
{
  "currentUser": {
    "id": "65454cd37af1ba29bfd92c00",
    "email": "boo@fax.com",
    "iat": 1699040684
  }
}
```

![current user found](./screenshots/currentuser-found-response.png)

Repeat the process and **delete the cookie**. Click the "Cookies" button in the
upper-right section of the Postman UI to access the available cookies for the
request. Send the request **again**, and verify that the `currentUser` property
is `null`:

```json
{
  "currentUser": null
}
```
