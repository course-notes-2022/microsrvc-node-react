# Current User Handler

Let's now move on to the `current-user` route handler:

![current user handler](./screenshots/current-user-handler.png)

The goal of this handler is to allow the client application to determine if the
current user is **authenticated to our backend**.

The client will make a request with the cookie, if it exists, to the `auth`
service's current-user route handler, we'll attempt to get the `req.session.jwt`
property on the cookie. If it is not set or invalid, we'll `return`. If the JWT
is valid, we'll return the **payload** from the JWT.
