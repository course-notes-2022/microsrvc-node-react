# Adding Session Support

Let's install `cookie-session` in the `auth` service:

`npm install cookie-session @types/cookie-session`

Update `auth/index.ts` as follows:

```ts
import express from 'express';
import { json } from 'body-parser';
import { currentUserRouter } from './routes/current-user';
import { signinRouter } from './routes/signin';
import { signupRouter } from './routes/signup';
import { signoutRouter } from './routes/signout';
import { errHandler } from './middleware/errHandler';
import mongoose from 'mongoose';
import cookieSession from 'cookie-session';

const app = express();

app.set('trust proxy', true); // Because traffic is being proxied through nginx; make express aware and trust proxy traffic
const PORT = 3000;

app.use(json());

// Use cookie-session middleware
app.use(
  cookieSession({
    signed: false,
    secure: true,
  })
);
app.use(currentUserRouter);
app.use(signinRouter);
app.use(signupRouter);
app.use(signoutRouter);
app.use(errHandler);

const start = async () => {
  try {
    await mongoose.connect('mongodb://auth-mongo-internal-srvc:27017/auth');
    console.log('connected to mongodb');
  } catch (err) {
    console.error(err);
  }
  app.listen(PORT, () => {
    console.log(`app listening on port ----> ${PORT}`);
  });
};

start();
```
