# Quick Signin Test

Let's test our signin functionality! First, we'll sign up a new user by sending
a `POST` request to `https://ticketing.dev/api/users/signup` with a valid
request body:

![sign-in test 1](./screenshots/signin-test-1.png)

Note that we get back the following value in the `session` cookie:

```
eyJqd3QiOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKcFpDSTZJalkxTkRVMFkyUXpOMkZtTVdKaE1qbGlabVE1TW1Nd01DSXNJbVZ0WVdsc0lqb2lZbTl2UUdaaGVDNWpiMjBpTENKcFlYUWlPakUyT1Rrd05EQTBOamQ5LkR2eFp6MDJBZkRWeERzUkFNVkhBUmhwaXlncjY0Yk9tanBFMW9rNzEyeGMifQ%3D%3D
```

Then, send another `POST` request to sign in the user at
``https://ticketing.dev/api/users/signup`:

![sign-in test 2](./screenshots/signin-test-2.png)

Note that we get back a **different** value in the cookie

```
eyJqd3QiOiJleUpoYkdjaU9pSklVekkxTmlJc0luUjVjQ0k2SWtwWFZDSjkuZXlKcFpDSTZJalkxTkRVMFkyUXpOMkZtTVdKaE1qbGlabVE1TW1Nd01DSXNJbVZ0WVdsc0lqb2lZbTl2UUdaaGVDNWpiMjBpTENKcFlYUWlPakUyT1Rrd05EQTJPRFI5Lk16QjA0QzhvdDlZVWh6Qm1JaFp2U05qWS1HaVZsSXlzWDZESlY1NWJBaDAifQ%3D%3D
```
