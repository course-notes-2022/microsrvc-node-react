# Cookies and Encryption

We've now established that we'll use a JWT as the auth mechanism, and use
cookies as the storage/management mechanism:

![jwt and cookies](./screenshots/jwt-and-cookies.png)

We'll use a helper library to manage our cookies,
[`cookie-session`](https://www.npmjs.com/package/cookie-session).

Cookies can be challenging to handle **across languages** because the contents
are often **encrypted**. This can be trouble because one library
(cookie-session, for example) can encrypt a cookie in such a way that a Ruby on
Rails app cannot understand.

We'll get around this by **not encrypting the cookie contents**. This is not a
big deal for us because JWTs are **naturally tamper-resistant**. We can easily
see whether a user attempted to **modify a JWT**, which is our main concern.
