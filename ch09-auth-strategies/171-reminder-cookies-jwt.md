# Reminder on Cookies vs. JWTs

Let's start to look at how we're going to prove a user is authenticated. First,
let's look at the difference between **cookies** and **JWT**s.

## Cookies

When a server sends a response to a client, it can optionally send a
`Set-Cookie` header back to the client. The cookie value can be a stirng that
contains any information we want. This information will be automatically stored
by the browser. Any **subsequent requests to the same domain** will be sent in
the **Request** headers along with the request.

## JWT

A **JWT** is an aribitrary piece of information called the **payload**. We take
the payload and run it through a **JWT creation algorithm**. We can easily take
the token and **decode** it using a decoding algorithm to extract the **original
payload**.

Once the browser receives the token, it must transimt it to the server to prove
authentication. The token can go in the `Authorization` header, in the request
body, or both.

| Cookies                                           | JWT                                    |
| ------------------------------------------------- | -------------------------------------- |
| Transport mechanism between browser and server    | Authentication/Authorization mechanism |
| Moves any kind of data between browser and server | Stores any data we want                |
| Automatically managed by the browser              | We have to manage it manually          |
