# The Signin Flow

We are now done with the signup flow. Let's now turn to the signin flow:

1. User sends a signin request with an email and password.

2. Auth service checks if a user with the email exists. If **no**, respond with
   an error.

3. Auth service compares the passwords of the stored user and the supplied
   password. If passwords **match** user is authenticated.

4. Send a JWT in a cookie to the authencticated user.

Add the following to `signin.ts`:

```ts
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { RequestValidationError } from '../errors/request-validation-err';

const router = express.Router();

router.post(
  '/api/users/signin',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .notEmpty()
      .withMessage('You must supply a password'),
  ],
  (req: Request, res: Response) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      throw new RequestValidationError(errors.array());
    }
  }
);

export { router as signinRouter };
```

Send a new `POST` request to `https://ticketing.dev/api/users/signin` with an
invalid body. Verify that the appropriate error response is returned.
