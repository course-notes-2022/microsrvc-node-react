# So Which Option Will We Choose?

We're going to choose option 2, because we want to promote the idea of
**independent services**. Note that we could definitely have a hybrid approach.
