# Common Response Properties

Note now that we're sending the **complete `User` model** in our response body.
We may not want to send **all this information**, especially the password.

Recall that in a microservice environment, each service may have its own
language and its own **database**.

Mongodb records the ID attribute of the record as `_id`. Other databases do not.
We want to ensure that our **client applications** get a **consistent response**
that is **not specific to any particular database implementation**.

Let's learn how to handle this in a **central location** in our application code
next.
