# Generating a JWT

Let's learn to **generate our JWT** and store it on our cookie.

We'll use the `req.session` property created by `cookie-session`. It is an
object, and any info we store on it will be **serialized** and stored **inside
the cookie**. This is how we'll store the JWT in the cookie:

```js
var cookieSession = require('cookie-session');
var express = require('express');

var app = express();

app.set('trust proxy', 1); // trust first proxy

app.use(
  cookieSession({
    name: 'session',
    keys: ['key1', 'key2'],
  })
);

app.get('/', function (req, res, next) {
  // Update views
  req.session.views = (req.session.views || 0) + 1;

  // Write response
  res.end(req.session.views + ' views');
});

app.listen(3000);
```

Let's use the [`jsonwebtoken`](https://www.npmjs.com/package/jsonwebtoken)
package to **generate our tokens**. Note that the library has a method `verify`:
we'll use this to ensure that the token hasn't been tampered with:

> `npm install jsonwebtoken @types/jsonwebtoken`

Refactor `auth/src/routes/signup.ts` as follows:

```ts
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { RequestValidationError } from '../errors/request-validation-err';
import { User } from '../models/user';
import { BadRequestError } from '../errors/bad-request-error';
import jwt from 'jsonwebtoken';

const router = express.Router();

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw new RequestValidationError(errors.array());
    }

    const { email, password } = req.body;
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new BadRequestError('email in use');
    } else {
      const user = User.build({ email, password });
      await user.save();

      // Generate JWT
      const userJwt = jwt.sign(
        {
          id: user.id,
          email: user.email,
        },
        'asdf'
      );

      // Store it on session object
      // Create a new object to workaround
      // Typescript definitions
      req.session = {
        jwt: userJwt,
      };

      return res.status(201).json(user);
    }
  }
);

export { router as signupRouter };
```

Save your changes and test in Postman with a `POST` request to
`http://ticketing.dev/api/users/signup`. Note that the **response** succeeds,
but we get **no cookies** in the "Cookies" tab:

![no cookies](./screenshots/no-cookies.png)

Recall that we configured `cookie-session` to **not** manage any cookies if the
user is **not** connecting via `https`. Try the request **again** with HTTPS:

![yes cookies](./screenshots/yes-cookies.png)

Now, anytime we make another request to `https://ticketing.dev`, **our cookie
will be included on the request and the request will contain our JWT**.
