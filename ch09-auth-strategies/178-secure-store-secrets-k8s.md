# Securely Storing Secrets with K8s

We need to make the JWT signing key available to all our services. To do so,
we'll use the K8s `Secret` object. A `Secret` allows us to store key-value pairs
of information. We can then load the secret into the containers in our pods. The
secret is exposed as an **environment variable** inside the container.
