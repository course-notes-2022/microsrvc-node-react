# Solving Issues with Option 2

Let's look at one strategy we could use to overcome the problem of latency in
communicating expired/unauthorized user credentials.

**Note that we are not actually going to implement this strategy.**

The "magic" is to **expire the token** after a certain period of time (e.g. 15
minutes):

![token good for 15](./screenshots/token-good-for-15.png)

If the user presents an **expired token**, the user is **not authenticated**.
From there we can:

- Reach out to the `auth` service and get a **refresh token** and send back to
  the client:

![refresh token](./screenshots/refresh-token.png)

- Reject the request if the token is expired, and require the **client** to
  re-authenticate and obtain a new token

## Making Sure Unauthorized Users are Denied Access ASAP

We can follow an alternate approach in situations where we want to **deny
revoked users access** to our application **as soon as possible**:

1. Admin user makes a request to ban user ABC

2. Auth service reaches out to DB to mark ABC as banned

3. Auth service ALSO emits a `UserBanned` event to event bus to **deny access to
   ABC**.

4. Event bus emits `UserBanned` event to all services

5. Each service maintains a **short-lived**, in-memory cache of banned users. We
   only need to persist the data as long as the token lives. Afterwards, the
   token is expired and the **service will contain logic to immediately revoke
   expired tokens**:

![asap ban](./screenshots/asap-ban.png)
