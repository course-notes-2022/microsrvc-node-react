# Creating and Accessing Secrets

Let's learn how to create a K8s `Secret`. We can use the following command:

> `kubectl create secret generic <secret-name> --from-literal=jwt=asdf`

This is an **imperative command** in K8s, i.e. one that **directly creates**
objects. The reason we're doing this is because we **do not want a config file
in which our secret is exposed**.

Note that this method requires us to **remember all the secrets we've created
every time we start up a new cluster**. A better solution for production is to
use a config file.

In the `auth-depl.yaml` config file, add the following:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth-depl
spec:
  replicas: 1
  selector:
    matchLabels:
      app: auth-depl
  template:
    metadata:
      labels:
        app: auth-depl
    spec:
      containers:
        - name: auth
          image: jfarrow02/auth-ms
          env: # Configure ENV VAR in the deployment container with this configuration
            - name: JWT_KEY
              valueFrom:
                secretKeyRef:
                  name: jwt-secret
                  key: signing-key

---
apiVersion: v1
kind: Service
metadata:
  name: auth-ms-internal
spec:
  type: ClusterIP
  selector:
    app: auth-depl
  ports:
    - name: auth
      protocol: TCP
      port: 3000
      targetPort: 3000
```

We have created our secret, and bound the value to an ENV VAR inside our
container.
