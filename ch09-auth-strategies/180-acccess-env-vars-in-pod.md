# Accessing Env Variables in a Pod

We can now access the environment variable we just created from our `Secret`
**inside the code** of our `auth` application running in our container/pod.

Make the following changes to `index.ts`:

```ts
import express from 'express';
import { json } from 'body-parser';
import { currentUserRouter } from './routes/current-user';
import { signinRouter } from './routes/signin';
import { signupRouter } from './routes/signup';
import { signoutRouter } from './routes/signout';
import { errHandler } from './middleware/errHandler';
import mongoose from 'mongoose';
import cookieSession from 'cookie-session';

const app = express();
app.set('trust proxy', true);
const PORT = 3000;

app.use(json());
app.use(
  cookieSession({
    signed: false,
    secure: true,
  })
);
app.use(currentUserRouter);
app.use(signinRouter);
app.use(signupRouter);
app.use(signoutRouter);
app.use(errHandler);

const start = async () => {
  // Check that JWT_KEY env var is defined
  if (!process.env.JWT_KEY) throw new Error('JWT_KEY must be defined');
  try {
    await mongoose.connect('mongodb://auth-mongo-internal-srvc:27017/auth');
    console.log('connected to mongodb');
  } catch (err) {
    console.error(err);
  }
  app.listen(PORT, () => {
    console.log(`app listening on port ----> ${PORT}`);
  });
};

start();
```

Add the following to `signup.ts`:

```ts
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { RequestValidationError } from '../errors/request-validation-err';
import { User } from '../models/user';
import { BadRequestError } from '../errors/bad-request-error';
import jwt from 'jsonwebtoken';

const router = express.Router();

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw new RequestValidationError(errors.array());
    }

    const { email, password } = req.body;
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new BadRequestError('email in use');
    } else {
      const user = User.build({ email, password });
      await user.save();

      const userJwt = jwt.sign(
        {
          id: user.id,
          email: user.email,
        },
        // USE JWT_KEY env var
        process.env.JWT_KEY! // "!": Tells Typescript that we know JWT_KEY is defined
      );

      req.session = {
        jwt: userJwt,
      };

      return res.status(201).json(user);
    }
  }
);

export { router as signupRouter };
```

Test again in Postman and confirm that the response is successful and contains
an JWT in the `session` cookie.
