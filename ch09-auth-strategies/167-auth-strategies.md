# Fundamental Authentication Strategies

In this chapter, we'll now learn how to **authenticate our users**. We will send
a cookie, JWT, or something to indicate that the user is authenticated, so that
the user can access our services.

## A Disclaimer

Handling authentication in a **microservice architecture** is **deceptively
tricky**. There are many ways to do it, and no "right" way. We'll take a look at
a couple of options and propose a solution that _works, but still has
downsides_.

## Eventual State

Imagine that we have an `OrdersService` that contains ticket purchasing logic. A
user sends a request to the service, along with some token that proves they are
signed in. Inside the service, we need to have logic to determine if the user is
**really logged in or not**.

Let's look at **two** broad approaches.

## Approach 1: Individual Services Rely on the Auth Service

![approach 1](./screenshots/approach-1.png)

We allow individual services to rely on a **centralized auth service** to
determine whether or not the user is logged in. The auth service will decide,
and send a response back to the calling service. The calling service sends a
**synchronous request** to the auth service, i.e. a **direct request** making no
use of event buses or message brokers.

The downside of this approach is that if the auth service ever goes **offline**,
_everything dependent on the auth service will break_.

## Approach 2: Individual services know how to authenticate a user

In this approach, **each service** contains logic to inspect the token and
decide whether the user is authenticated:

![approach 2](./screenshots/approach-2.png)

With this approach, although we remove the dependencies on the auth service, we
run into some **large** downsides which we'll look at in the next lesson.
