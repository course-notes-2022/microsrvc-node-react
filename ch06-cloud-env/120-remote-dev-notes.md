# Notes on Remote Development

**NOTE: This section is an _OPTIONAL_ section on how to set up a _remote_
development environment in Google Cloud**. The reason for this section is that,
for users on older machines (especially Windows), you _may_ experience crashes
as we will be working with a significant number of files. Again, this sections
does _not_ mandatory, and does _not_ have to be done **right now**. If you are
on a **newer machine** and are confident that you have enough RAM, **feel free
to skip this section**!

## Using Google Cloud

To use Google Cloud, you _must_ have a credit card. At present, the free tier
lasts for 1 year. Everything we will do in this course can be satisfied under
the free tier.

We will create **separate files** for each route handler function in our
application (note that this has nothing to do with microservices _per se_, but
is simply an organizational decision):

```
|-auth
|--/src
|----/routes
|------signin.ts
|------signout.ts
|------signup.ts
|------current-user.ts
|--index.ts

```

`current-user.ts`:

```javascript
import express from 'express';

const router = express.Router();

router.get('/api/users/currentuser', () => {});

export { router as currentUserRouter };
```

`server.ts`:

```javascript
// ...
import { currentUserRouter } from './routes/current-user';

const server = express();
server.use(json());

server.use(currentUserRouter);
```
