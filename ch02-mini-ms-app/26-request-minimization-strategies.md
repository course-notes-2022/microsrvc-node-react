# Request Minimization Strategies

Our app is working, but it's not optimal. We are able to see all our posts, and
make comments. But if you inspect the network tab, we are making a network
request **for each comment that we fetch**. This consumes unnecessary bandwidth
and **slows our application down**. How can we make a **single request** and get
all the posts _and_ all the associated comments?

In a **monolith application**, this would be easy:

![request from monolith](./screenshots/req-from-monolith.png)

But in a **microservice architecture**, it's more complicated. Right now, we
have a way to make a request to the `posts` service _or_ the `comments` service
_only_. How do we solve this? We have two possible solutions:

## Solution 1: Synchronous Communication

A **synchronous solution** might involve the following:

1. Send a request from the client to `GET` all posts with associated comments
2. The `posts` service receives the request, and makes a \*\*second request to
   the `comments` service
3. `comments` responds with the comments data to `posts`
4. `posts` bundles and sends the complete response back to the client

![sync solution](./screenshots/sync-solution.png)

This approach has some significant downsides:

- Introduces a dependency between `posts` and `comments` services
- If any inter-service request fails, the overall request fails
- The entire request is only as fast as the slowest request
- Can easily introduce webs of requests
