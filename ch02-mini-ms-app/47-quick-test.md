# A Quick Test

Let's make sure that when query service receives a `CommentUpdated` event, it
finds the appropriate comment and updates its status. Update the query service's
`POST /events` handler as follows:

```js
app.post('/events', (req, res) => {
  const { type, data } = req.body;
  if (type === 'PostCreated') {
    const { id, title } = data;
    posts[id] = { id, title, comments: [] };
  }

  if (type === 'CommentCreated') {
    const { id, content, postId, status } = data;
    const post = posts[postId];
    post.comments.push({ id, content, status });
  }

  if (type === 'CommentUpdated') {
    const { id, content, postId, status } = data;

    const post = posts[postId];
    const comment = post.comments.find((comment) => {
      return comment.id === id;
    });
    comment.status = status; // Update status of comment
    comment.content = content; // Update content in case it has changed as well
  }

  console.log(posts);
  res.status(201).send({ received: true });
});
```

Verify that all your services are still running and there are no errors in the
terminal. Refresh the client application in the browser. Create a new post and a
new comment for the post that does **not** contain the word "orange". Verify the
following:

- `localhost:4000`: Verify that the `PostCreated`, `CommentCreated`,
  `CommentModerated`, `CommentUpdated` strings are logged to the console

- `localhost:4001`: Verify that the `PostCreated`, `CommentCreated`,
  `CommentModerated`, `CommentUpdated` strings are logged to the console

- `localhost:4002`: Verify that the post is logged to the console at each stage
  of the create/moderate/update cycle

- `localhost:4003`: No output
- `localhost:4005`: No output

Verify in the browser network tab that the comment status is `approved`:

```json
{
  "cbe92f28": {
    "id": "cbe92f28",
    "title": "OMG A POST",
    "comments": [
      {
        "id": "b316989b",
        "content": "OMG RLY THIS POST IZ GR8",
        "status": "approved"
      }
    ]
  }
}
```

![approved post](./screenshots/approved-post.png)
