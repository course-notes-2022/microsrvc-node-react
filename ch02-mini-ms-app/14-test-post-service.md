# Testing the `posts` Service

Later on in the course, we'll begin to write some **automated tests** for our
services. For now, we can simply test with `Postman`.

Send a `POST` request with the following body to your `posts` service on port
`4000`:

```json
{ "title": "First Post" }
```

Verify that the response status code is `201`, and the outupt is similar to the
following:

```json
{
  "id": "13eb2163",
  "title": "First Post"
}
```
