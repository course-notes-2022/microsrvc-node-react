# Implementing Event Sync

Let's try implementing event sync option #3. Note that there are many **real,
open-source** projects that will implement this functionality in a **much better
way**.

We'll:

- Take down the query service
- Create several posts and comments
- Restart the query service, and ensure that it can read the latest events from
  the event bus

Make the following changes to `event-bus`'s `index.js` file:

```js
const events = []; // Store every incoming event

app.post('/events', (req, res) => {
  const event = req.body;
  events.push(event); // Push new event into event store
  // Make POST request to other services
  axios.post('http://localhost:4000/events', event);
  axios.post('http://localhost:4001/events', event);
  axios.post('http://localhost:4002/events', event);
  axios.post('http://localhost:4003/events', event);
  res.send({ status: 'OK' });
});

// Add handler to retrieve event history
app.get('/events', (req, res) => {
  res.status(200).json(events);
});
```

Refactor the `query-service` `index.js` file as follows:

_Note: Make sure to install the `axios` package in `query service`_

```js
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = 4002;

const posts = {};

// Extract the event handling logic to a helper function
const handleEvent = (type, data) => {
  if (type === 'PostCreated') {
    const { id, title } = data;
    posts[id] = { id, title, comments: [] };
  }

  if (type === 'CommentCreated') {
    const {
      id,
      content,
      postId,
      status, // Destructure `status` property from incoming comment
    } = data;
    const post = posts[postId];
    post.comments.push({ id, content, status });
  }

  if (type === 'CommentUpdated') {
    const { id, content, postId, status } = data;

    const post = posts[postId];
    const comment = post.comments.find((comment) => {
      return comment.id === id;
    });
    comment.status = status;
    comment.content = content; // Update content in case it has changed as well
  }
};

app.use(bodyParser.json());
app.use(cors());

app.get('/posts', (req, res) => {
  res.send(posts);
});

app.post('/events', (req, res) => {
  const { type, data } = req.body;
  // Call the event handling helper
  handleEvent(type, data);
  res.status(201).send({ received: true });
});

app.listen(PORT, async () => {
  console.log(`app listening on port ${PORT}`);

  // Request the event history from the event bus
  // as soon as query service starts
  const res = await axios.get('http://localhost:4005/events');
  for (let event of res.data) {
    console.log('Processing event: ', event.type);
    handleEvent(event.type, event.data);
  }
});
```
