# An Async Solution

A second possible solution would involve **async communication**. Async
communication involves the use of an **event broker**. The event broker's job is
to:

- **Receive** events
- **Forward events on** to interested parties

We are going to introduce a **query service**, that listens for **events**
emitted by the `post` and `comments` services that indicate that a post and/or
comment has been created. The query service will then **assemble the data** into
the desired structure and **send to the client** in one request.

## Pros and Cons

### Pros

- Query service has zero dependencies on other services
- Query service will be fast!

### Cons

- Data duplication
- Harder to implement
