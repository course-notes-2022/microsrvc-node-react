# Parsing Incoming Events

Let's now create the `query` service and give it the ability to receive events
from the `posts` and `comments` services (via the event bus):

```js
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = 4002;

const posts = {};
/*
posts = {
        'jser34r3': {
            id: 'jser34r3',
            title: 'post title',
            comments: [
                {
                    id: 'ke203804r',
                    content: 'some content'
                }
            ]
        }
    }
*/

app.use(bodyParser.json());
app.use(cors());

app.get('/posts', (req, res) => {
  res.send(posts);
});

app.post('/events', (req, res) => {
  const { type, data } = req.body;
  if (type === 'PostCreated') {
    const { id, title } = data;
    posts[id] = { id, title, comments: [] };
  }

  if (type === 'CommentCreated') {
    const { id, content, postId } = data;
    const post = posts[postId];
    post.comments.push({ id, content });
  }

  console.log(posts);
  res.status(201).send({ received: true });
});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
```

Start the `query` service in a terminal. Create a new post and comment for the
post. Verify the query service logs information similar to the following:

```
app listening on port 4002
{ '4a697a88': { id: '4a697a88', title: 'Hello World', comments: [] } }
{
  '4a697a88': { id: '4a697a88', title: 'Hello World', comments: [ [Object] ] }
}

```
