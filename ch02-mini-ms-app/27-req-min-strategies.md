# Request Minimization Strategies

Currently the demo app makes a single request for every comment of every post.
This is sub-optimal. There are a couple of possible solutions:

## Synchronous Communication Solution:

The `posts` service commuicates **directly** with the `comments` service
whenever the server receives a `GET` request for `posts`:

![sync solution](./sync_solution.png)

## Asynchronous Communication Solution

An **event broker** receives notifications from different services and routes
them to other interested services.

- The `posts` service emits an event any time a post is created.
- The `comments` service emits an event any time a comment is created.
- The `query` service assembles all the posts and comments into an efficeint
  data structure.
- The `event broker` receives events and sends them to interested parties.
