# Handling Moderation

Let's now moderate our new comment. Make the following updates in `moderation`
service:

```js
app.post('/events', async (req, res) => {
  const { type, data } = req.body;

  if (type === 'CommentCreated') {
    const status = data.content.includes('orange') ? 'rejected' : 'approved'; // Moderation!

    // Send `CommentModerated` event to event bus
    await axios.post('http://localhost:4005/events', {
      type: 'CommentModerated',
      data: {
        id: data.id,
        postId: data.postId,
        status,
        content: data.content,
      },
    });
  }

  res.status(201).json({});
});
```

Publish event _back_ to `moderation` service (because the event bus publishes to
_all_ services, regardless of whether they're interested or not):

```js
app.post('/events', (req, res) => {
  const event = req.body;
  axios.post('http://localhost:4000/events', event);
  axios.post('http://localhost:4001/events', event);
  axios.post('http://localhost:4002/events', event);
  axios.post('http://localhost:4003/events', event); // Publish to moderation
  res.send({ status: 'OK' });
});
```
