# Adding a Simple Feature

Let's add a feature to simulate comment moderation. We'll flag comments that
contain the word 'orange'.

We could implement this on the client side easily, but not if the filter list
changes frequently. It would also be easy to implement this in the existing
`comments` service, but we'll assume we want to add a new service. We'll also
assume that there is significant **latency** when moderating a comment.

The client app needs to be able to know the state of a comment: pending
moderation, rejected, or approved:

`Comment`:

| name    | type                              |
| ------- | --------------------------------- |
| id      | string                            |
| content | string                            |
| status  | 'approved', 'rejected', 'pending' |
