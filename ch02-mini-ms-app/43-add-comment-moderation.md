# Adding Comment Moderation

Let's now add comment moderation functionality. We'll follow the workflow of the
client submitting a new comment for a post.

## Step 1: Create a New Comment

Update the `comment` service's `/posts/:id/comments` handler:

```js
app.post('/posts/:id/comments', async (req, res) => {
  const commentId = randomBytes(4).toString('hex');
  const { content } = req.body;

  const comments = commentsByPostId[req.params.id] || [];

  comments.push({
    id: commentId,
    content,
    status: 'pending', // Add the `status` property to each new comment
  });
  commentsByPostId[req.params.id] = comments;

  await axios.post('http://localhost:4005/events', {
    type: 'CommentCreated',
    data: {
      id: commentId,
      content,
      postId: req.params.id,
      status: 'pending', // Add the `status` property to the comment passed to the query service
    },
  });
  res.status(201).json(comments);
});
```

## Step 2: Fetch the Updated Comment in Query Service

Add the following to the `query` service's to handle the new `status` property
on a pending comment from `comment` service:

```js
app.post('/events', (req, res) => {
  const { type, data } = req.body;
  if (type === 'PostCreated') {
    const { id, title } = data;
    posts[id] = { id, title, comments: [] };
  }

  if (type === 'CommentCreated') {
    const {
      id,
      content,
      postId,
      status, // Destructure `status` property from incoming comment
    } = data;
    const post = posts[postId];
    post.comments.push({
      id,
      content,
      status, // Add status property
    });
  }
  console.log(posts);
  res.status(201).send({ received: true });
});
```

Next, we'll look at moderating the comment (i.e. searching for the string
'orange' and updating the status) in the moderation service next.
