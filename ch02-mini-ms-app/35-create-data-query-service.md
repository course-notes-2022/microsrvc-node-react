# Creating the Data Query Service

Let's work on the `query` service. We will give it two route handlers:

- `GET` /posts: Provide a full listing of posts and comments
- `POST` /events: Receives `PostCreated` and `CommentCreated` events from event
  bus and assembles the data into a bundle

Initialize a new `npm` project in the `query` directory. Install `express` and
`cors`. Add the following content to `index.js`:

```js
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');

const PORT = 4002;

app.use(bodyParser.json());
app.use(cors());

app.get('/posts', (req, res) => {});

app.post('/events', (req, res) => {});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
```

Add the `start` and `dev` scripts to `package.json`.
