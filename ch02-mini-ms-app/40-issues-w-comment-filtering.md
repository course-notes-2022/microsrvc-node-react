# Issues w/ Comment Filtering

**New feature**: We want to moderate comments and filter all comments that
contain the word "orange".

We have several options for implementing this feature:

## Option 1: Moderation Service Communicates Event Creation to Query Service

1. User submits comment to Comments srvc
2. Comments service creates new record in **its own** db
3. Comments srvc emits event to event bus
4. Event bus emits event to **all services** that care about the event
5. Moderation srvc receives event from event bus and decides whether to
   approve/reject the comment
6. Moderation srvc emits `CommentModerated` event, including approved/rejected
   status, to event bus
7. Event bus emits `CommentModerated` event to all services (e.g. Query Service)
8. Query service will persist the comment with the approved status

**Cons**:

- **Delay** between user commiting comment and approval by `ModerationService`.
  Remaining workflow is **paused** while moderation is pending. User will not
  see new comment until the process is complete

## Option 2: Moderation Updates Status at Both Comments and Query Services

1. User submits comment to Comments srvc
2. Comments service creates new record in **its own** db
3. Comments srvc emits event to event bus
4. Event bus emits event to **all services** that care about the event, incl.
   `ModerationService` **and** `QueryService`.
5. `QueryService` persists info about comment, **including** default status of
   `pending`.
6. `ModerationService` emits a `CommentModerated` event to event bus once
   approval/rejection is complete.
7. `QueryService` updates its record once it receives `CommentModerated` event
   from event bus.

**Cons**:

- `QueryService` is only about presentation logic. Does it make sense for a
  presentation service to understand how to process this **very precise**
  update? What if there are **more** business logic updates that we need to make
  later?

  ![precise updates](./screenshots/precise-updates.png)

## Option 3: Comment Moderation is Processed by `CommentService`

![option 3](./option_3.png)

1. User submits comment to Comments srvc
2. Comments service creates new record in **its own** db, **including comment
   status**
3. `CommentService` emits event to event bus --> `ModerationService` and
   `QueryService`
4. `ModerationService` continues to moderate comment and approve/reject; sends
   event to event bus --> event bus to `CommentService`
5. `CommentService` updates comment status and emits `CommentUpdated` event to
   event bus
6. `QueryService` is notified by event but and saves all comment attributes
