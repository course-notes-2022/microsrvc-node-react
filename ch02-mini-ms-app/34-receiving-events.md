# Receiving Events

Let's now refactor our `post` and `comments` services to receive events from the
event bus (and fix the `404` errors we saw earlier). Add the following request
handler method to both services:

```js
// Receive event from event bus
app.post('/events', (req, res) => {
  console.log(`received event: ${req.body.type}`);
  res.status(201).json({ received: true });
});
```

Test the post creation flow in the browser client. Verify that the request
succeeds, and that the `posts` and `comments` services log:

```
Received Event PostCreated
```

Verify the same for the `CommentCreated` event. Our post and comment services
are now being **notified** by the event bus that events are taking place!
Neither service really _cares_ about these events, but this means we can now
create the `query` service that _does care_, and can package the event data for
sending **back to the client**.
