# `comments` Service

Let's now begin working on the `comments` service:

| Path                  | Method | Body                | Goal                                                    |
| --------------------- | ------ | ------------------- | ------------------------------------------------------- |
| `/posts/:id/comments` | `POST` | `{content: string}` | Create a new comment associated with the given post ID  |
| `/posts/:id/comments` | `GET`  | _none_              | Retrieve all comments associated with the given post ID |

Create an `index.js` file and add the following:

```js
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const { randomBytes } = require('crypto');
const PORT = 4001;

const commentsByPostId = {};
/*
{
    abc123: [
        {commentid: '123xyz', comment: 'This is a comment'},
        {commentid: '123abc', comment: 'This is another comment'}
    ]
}
*/

app.use(bodyParser.json());

app.get('/posts/:id/comments', (req, res) => {
  res.status(200).json(commentsByPostId[req.params.id] || []);
});

app.post('/posts/:id/comments', (req, res) => {
  const commentId = randomBytes(4).toString('hex');
  const { content } = req.body;

  const comments = commentsByPostId[req.params.id] || [];
  comments.push({ id: commentId, content });
  commentsByPostId[req.params.id] = comments;
  res.status(201).json(comments);
});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
```

Add the same `start` and `dev` scripts to `package.json` as you did for the
`posts` service.
