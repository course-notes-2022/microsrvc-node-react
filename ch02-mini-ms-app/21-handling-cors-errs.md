# Handling CORS Errors

CORS ("Cross-Origin Resource Sharing") is a browser rule that prevents **sharing
resources across origins**, i.e. domains. Basically, it prevents an application
running in domain A from accessing assets in an application running in domain B.
CORS is an important security feature of the internet.

For an API that we want to make available to the public, we need to ensure that
we send **a specific set of headers** that indicate we want to **allow** apps in
other domains to access our API.

We can do this easily in `node` with the `cors` package. In each `index.js` file
of our microservices, `require` the `cors` package, and use it as middleware:

```js
app.use(cors());
```

Be sure to call `cors` as a function. That's it! Now, when we send another
`POST` request to create a post from the client, we should see the request
succeed:

![create post success](./screenshots/create-post-success.png)
