# Create Comments

Let's now add the functionality to create comments for a post. Add the following
to the `comment-create` component file:

```ts
import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { CommentService } from '../comment-service';
import { v4 as uuidv4 } from 'uuid';
import { Comment } from '../Comment';

@Component({
  selector: 'app-comment-create',
  templateUrl: './comment-create.component.html',
  styleUrls: ['./comment-create.component.css'],
})
export class CommentCreateComponent implements OnInit {
  constructor(private commentService: CommentService) {}

  @Input()
  postId: string = '';
  commentForm: FormGroup = new FormGroup({});

  onSubmit() {
    this.commentService
      .createCommentForPost(
        this.postId,
        new Comment(uuidv4(), this.commentForm.value.content)
      )
      .subscribe((response: any) => {
        console.log(response);
      });
  }

  ngOnInit(): void {
    this.commentForm = new FormGroup({
      content: new FormControl(null),
    });
  }
}
```

Note that the component expects to receive the `postId` as an **input prop**.

Add the following to the `comment-create` template:

```html
<div class="comment-create">
  <form [formGroup]="commentForm" (ngSubmit)="onSubmit()">
    <div class="form-group">
      <label>New Comment</label>
      <input class="form-control" formControlName="content" />
    </div>
    <button class="btn btn-primary">Submit</button>
  </form>
</div>
```

Ensure that `CommentService` methods are expecting to receive the `postId`:

```ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Comment } from './Comment';

@Injectable()
export class CommentService {
  constructor(private client: HttpClient) {}

  getCommentsForPost(postId: string): Observable<Object> {
    return this.client.get(`http://localhost:4001/posts/${postId}/comments`, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  createCommentForPost(postId: string, comment: Comment): Observable<Object> {
    return this.client.post(
      `http://localhost:4001/posts/${postId}/comments`,
      comment
    );
  }
}
```

Add the following to the `comment-list` component:

```ts
import { Component, Input, OnInit } from '@angular/core';
import { CommentService } from '../comment-service';
import { Comment } from '../Comment';
@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css'],
})
export class CommentListComponent implements OnInit {
  constructor(private commentService: CommentService) {}

  @Input()
  postId: string = '';

  comments: Comment[] = [];

  ngOnInit(): void {
    this.commentService
      .getCommentsForPost(this.postId)
      .subscribe((data: any) => {
        console.log(data);
        this.comments = data as Comment[];
      });
  }
}
```

Again, we pass the `postId` as an **input prop** from the parent, so that each
comment knows **to which post it belongs**.

Create a `<li>` element for each comment on the post in the `comment-list`
template:

```html
<ul>
  <li *ngFor="let c of comments">{{ c.content }}</li>
</ul>
```

Finally, output both the `comment-list` and the `comment-create` form **inside
the `post-list` template** for each post:

```html
<div class="post-list d-flex flex-row flex-wrap justify-content-between">
  <div
    class="card"
    *ngFor="let p of postsArray"
    style="width: 30%; margin-bottom: 20px"
  >
    <div class="card-body">
      <h3>{{ p.title }}</h3>
      <app-comment-list [postId]="p.id"></app-comment-list>
      <app-comment-create [postId]="p.id"></app-comment-create>
    </div>
  </div>
</div>
```

Test your changes in a browser. Remember to refresh the page to see your
changes.
