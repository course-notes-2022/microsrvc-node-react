# Dealing with Missing Events

Consider the following scenario:

![missing event](./missing_events.png)

The `QueryService` comes online **after** the remaining services have already
been implemented. How does `QueryService` know about events that occurred
**before it was created**?

There are several solutions:

## Option 1: Sync Requests

Code inside `QueryService` could make a synchronous request to both `Posts` and
`Comments` services to fetch all posts and and comments:

![sync requests](./sync_requests.png)

**Cons**:

- We would have to have code inside `Posts` and `Comments` **just** to handle
  these requests when `QueryService` comes online

## Option 2: Direct DB Access

![direct db access](./direct_db_access.png)

`QueryService` has direct access to the other services' DBs, and can query them
all when it comes online.

**Cons**:

- We would have to implement code inside `QueryService` to interface with
  **all** relevant services' DBs. What if different services use different DBs?
  We have to write code to interface with them all!

## Option 3: Store Events (\*Preferred)

![store events](./store_events.png)

The `EventBus` will **store every single event internally** in its own **data
store**, e.g. a database (as this store will grow very large over
time<sup>\*</sup>). Event bus still emits all events to other services as
normal. Once `QueryService` comes online, `EventBus` emits **all** its stored
events to `QueryService`.

<sup>\*</sup> _There will be cost associated with the storage of all this data,
but likely it'll be manageable_.
