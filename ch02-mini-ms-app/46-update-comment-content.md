# Updating Comment Content

Let's make sure that the comment service watches for the `CommentModerated`
event, and updates the comment status upon receiving the event update:

![comment moderated](./screenshots/comment-moderated.png)

Make the following changes in the `comment` service to notify the service that
the comment status has been moderated by the `moderation` service:

```js
app.post('/events', async (req, res) => {
  console.log(`received event: ${req.body.type}`);
  const { type, data } = req.body;
  if (type === 'CommentModerated') {
    const { postid, content, id, status } = data;
    const comments = commentsByPostId[postId];
    const comment = comments.find((comment) => {
      return comment.id === id;
    });
    comment.status = status;

    // Send CommentUpdated event to event bus
    await axios.post('http://localhost:4005', {
      type: 'CommentUpdated',
      data: {
        id,
        status,
        postId,
        content,
      },
    });
  }
  res.status(201).json({ received: true });
});
```
