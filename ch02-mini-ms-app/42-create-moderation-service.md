## Creating the Moderation Service

Now that we've selected option 3, we need to make several changes, including
creating the `moderation` service. We'll start there.

Create the `moderation` directory in the project root and initialize a new
project. Install `express`, `nodemon`, and `axios` (`cors` is not needed). Add
the following to `index.js`:

```js
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const axios = require('axios');
const PORT = 4003;

app.use(bodyParser.json());

app.post('/events', (req, res) => {});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
```
