# Quick Comments Test

Let's do a quick Postman test for the `comments` service. Start up the service
on port `4001` in a terminal, and send a `POST` request with the following body:

```json
{
  "content": "This is a great post!"
}
```

Observe that we get a `201` response with the following body:

```json
[
  {
    "id": "47bd77e0",
    "content": "This is a great post!"
  }
]
```

Send a `GET` request, and observe that our output is as expected:

```json
[
  {
    "id": "47bd77e0",
    "content": "This is a great post!"
  }
]
```
