# Addressing Default Export and ReactDom.render Warnings

In the upcoming lecture, we will be creating our first React component. You will
see a few warnings in the terminal or browser console:

```
Line 1:1: Assign arrow function to a variable before exporting as module default import/no-anonymous-default-export
```

This is a linter warning as of React v17 letting us know that it might be wise
to use named exports instead.

You can suppress the warning by refactoring from this:

```ts
import React from "react";

export default () => {
  return <div>Blog app</div>;
};
to this:

import React from "react";

const App = () => {
  return <div>Blog app</div>;
};

export default App;

```

The warning will come up a few more times in this project (and throughout the
course) when creating components and can be handled similarly.

```
Warning: ReactDOM.render is no longer supported in React 18. Use createRoot instead.
```

To address this warning, update the root index.js to use ReactDOM.createRoot
instead of ReactDOM.render:

```ts
import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
```
