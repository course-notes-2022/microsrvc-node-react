# Application Overview

- Goal 1: Get a taste of ms architecture
- Goal 2: Build from scratch as much as possible

**Do not use the project as a template for future MSs (we will build a better
template later)**

## What Services Should we Create?

For now, we will create one service for **each resource** in the app:

- Posts: Create/list posts
- Comments Create/list all comments for a post

![project 1 structure](./screenshots/project-1-structure.png)
