# Build `post` Submission

Create a new service `PostService` for communicating with the `posts` ms from
the Angular client:

```ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './Post';
import { Observable } from 'rxjs';
@Injectable()
export class PostService {
  constructor(private client: HttpClient) {}

  getPosts(): Observable<Object> {
    return this.client.get('http://localhost:4000/posts', {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  createPost(post: Post): Observable<Post> {
    return this.client.post<Post>('http://localhost:4000/posts', post);
  }
}
```

Add the following code to your `post-create` component file:

```ts
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PostService } from '../post-service';
import { Post } from '../Post';
@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css'],
})
export class PostCreateComponent implements OnInit {
  constructor(private postService: PostService) {}

  createPostForm: FormGroup = new FormGroup({});

  onSubmit() {
    console.log(this.createPostForm);
    this.postService
      .createPost(this.createPostForm.value)
      .subscribe((response: Post) => {
        console.log(response);
      });
  }

  ngOnInit(): void {
    this.createPostForm = new FormGroup({
      title: new FormControl(null),
    });
  }
}
```

Add the following to the `post-create` template:

```html
<div class="post-create">
  <h2>Create Post</h2>
  <form [formGroup]="createPostForm" (ngSubmit)="onSubmit()">
    <div class="form-group">
      <label>Title</label>
      <input formControlName="title" class="form-control" />
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
```

[Add the link to Bootstrap's CDN](https://getbootstrap.com/docs/5.3/getting-started/introduction/)
to the `head` tag of `index.html` to bring in the Bootstrap styling.

Start the Angular application with `ng serve` in a terminal window. Enter a
title in the input field and submit the form. Note that we get the following
error in the console:

![cors error](./screenshots/cors-err.png)

We'll fix this in the next lesson.
