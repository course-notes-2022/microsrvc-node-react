# Common Questions Around Async Events

| Question                                                                    | Answer                                                                                                                            |
| --------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| Do we need to create a new service _every time_ we want to join some data?! | No! In reality, might not even have posts and comments in separate services in the first place                                    |
| Who cares that each service is independent?                                 | Independent services + the reliability that brings is one of the **core reasons** ousing microservices in the first place         |
| This is too complicated, for too little benefit                             | It may seem that way _now_, but adding new features starts to get **really easy** when we use this architecture                   |
| This system won't work correctly in the following scenario...               | There are some special things we need to consider with theis design; there are solutions for most of the concerns you may have... |
