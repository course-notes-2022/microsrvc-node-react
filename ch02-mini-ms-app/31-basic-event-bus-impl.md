# Basic Event Bus Implementation

Let's begin building our custom event bus. Create a new project directory
`event-bus` in your project root, and initialize a new `npm` project. Install
`express`, `nodemon`, and `axios`.

Create an `index.js` file with the following content:

```js
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');

const app = express();
const PORT = 4005;

app.use(bodyParser.json());

app.post('/events', (req, res) => {
  const event = req.body;
  // Make POST request to other services
  axios.post('http://localhost:4000/events', event);
  axios.post('http://localhost:4001/events', event);
  axios.post('http://localhost:4002/events', event);

  res.send({ status: 'OK' });
});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
```

Add the "start" and "dev" scripts to `package.json` for starting the server.
