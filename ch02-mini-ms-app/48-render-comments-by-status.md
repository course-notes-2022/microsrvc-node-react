# Rendering Comments by Status

Let's refactor the client to reflect the changing comment status. Make the
following changes in the `comment-list` template:

```html
<ul>
  <li *ngFor="let c of comments">
    <p *ngIf="c.status === 'approved'">{{ c.content }}</p>
    <p *ngIf="c.status === 'pending'">
      <i>This comment is awaiting moderation</i>
    </p>
    <p *ngIf="c.status === 'rejected'">
      <i>Comment flagged for inappropriate content</i>
    </p>
  </li>
</ul>
```

To test the scenario in which moderation is taking place, we can simulate
moderation by **stopping the moderation service**.
