# Note on the React App

The next lectures are focused on building a React app to use our microservices.

**There is no content directly related to microservices in the lectures focused
on the React app. If you do not care about React, or do not want to use React,
then skip over the next 5 lectures.**

Find the lecture called "Completed React App" and download the completed code
from it. Make sure you read the text in that lecture as well, as there are some
important setup instructions.
