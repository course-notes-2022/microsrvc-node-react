# Emitting Comment Creation Events

Let's now refactor our `comment` service to communicate with the event bus on
creation of a comment:

```js
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const { randomBytes } = require('crypto');
const PORT = 4001;
const axios = require('axios');

const commentsByPostId = {};

app.use(bodyParser.json());
app.use(cors());

app.get('/posts/:id/comments', (req, res) => {
  res.status(200).json(commentsByPostId[req.params.id] || []);
});

app.post('/posts/:id/comments', async (req, res) => {
  const commentId = randomBytes(4).toString('hex');
  const { content } = req.body;

  const comments = commentsByPostId[req.params.id] || [];
  comments.push({ id: commentId, content });
  commentsByPostId[req.params.id] = comments;

  await axios.post('http://localhost:4005/events', {
    type: 'CommentCreated',
    data: { id: commentId, content, postId: req.params.id },
  });
  res.status(201).json(comments);
});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
```
