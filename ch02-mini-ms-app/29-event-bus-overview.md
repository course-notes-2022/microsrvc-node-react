# Event Bus Overview

There are many different implementations of an **event bus**: RabbitMQ, Kafka,
NATS etc. Event buses receive events and publish them to listeners. Certain
features of event bus implementations make async communication **easier** or
**harder**.

We are going to build a **simple** event bus using Express. For our next app we
will use a production grade, open-source event bus.

## Express Event Bus

Whenever an event is created, our event bus will emit an event to all the
services:

![express event bus 1](./screenshots/express_bus_one.png)

![express event bus 2](./screenshots/express_bus_two.png)
