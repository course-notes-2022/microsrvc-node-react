# Project Setup

Here is the general architecture of the app we're going to build:

![app architecture](./screenshots/app-architecture.png)

Notice that a _React_<sup>\*</sup> application will be running in the browser,
and communicate with a `Posts` service and a `Comments` service, both Express.js
apps. Remember that this is **an example app designed to help you understand
_how the various applications work together_**.

## Initial Setup

1. Generate a new React app with `create-react-app`
2. Create an Express project for the `Posts` service
3. Create an Express project for the `Comments` service.

Create the following directory structure:

```
| - /blog
    | - /client # React client app
    | - /posts # Posts service
    | - /comments # Comments service
```

Initialize a new `npm` project and install the following packages in `posts` and
`comments`:

- `express`
- `axios`
- `nodemon`
- `cors`

<sup>\*</sup>_Note: For this section, we're going to try and substitute the
React app for an **Angular** app, to get more practice building with Angular._
