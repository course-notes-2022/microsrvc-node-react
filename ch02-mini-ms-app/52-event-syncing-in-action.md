# Event Syncing in Action

Stop the query service. Let's now ensure that we can create posts, comments, and
then fetch those into `query` once it starts back up.

Remove or comment out the line below in `event-bus`:

```js
app.post('/events', (req, res) => {
  const event = req.body;
  events.push(event);
  axios.post('http://localhost:4000/events', event);
  axios.post('http://localhost:4001/events', event);
  // REMOVE THIS LINE
  // axios.post('http://localhost:4002/events', event);
  axios.post('http://localhost:4003/events', event);
  res.send({ status: 'OK' });
});
```

Create a new post in the client. Note that you will see errors in the console.
These are related to the query service being unavailable. You can still create a
post.

Restart `query` on port 4002. `query` will reach out to the event bus and
retrieve all the posts that it had missed. Verify that the output in the
terminal running the `query` service is similar to the following:

```
[nodemon] 3.0.1
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,cjs,json
[nodemon] starting `node index.js`
app listening on port 4002
Processing event:  PostCreated

```
