# Using the Query Service

Let's now refactor our client app to consume the **bundled data response** from
the `query` service, and minimize the requests sent to the api servers. Make the
following changes to `PostService`:

```ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Post } from './Post';
import { Observable } from 'rxjs';
@Injectable()
export class PostService {
  constructor(private client: HttpClient) {}

  getPosts(): Observable<Object> {
    return this.client.get('http://localhost:4000/posts', {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  createPost(post: Post): Observable<Post> {
    return this.client.post<Post>('http://localhost:4000/posts', post);
  }

  // This method fetches posts AND comments
  getPostsWithComments(): Observable<Object> {
    return this.client.get('http://localhost:4002/posts', {
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
```

Refactor the `post-list` component file to use the new method to fetch posts and
comments **together**:

```ts
import { Component, OnInit } from '@angular/core';
import { PostService } from '../post-service';
import { PostComment } from '../PostComment';
@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
})
export class PostListComponent implements OnInit {
  constructor(private postService: PostService) {}

  posts: { [key: string]: PostComment } = {};
  postsArray: PostComment[] = [];

  ngOnInit(): void {
    this.postService.getPostsWithComments().subscribe((data: Object) => {
      this.posts = data as { [key: string]: PostComment };
      for (const p in this.posts) {
        this.postsArray.push(this.posts[p]);
      }
      console.log(this.postsArray);
    });
  }
}
```

Refactor the `post-list` template to pass only the **comments** for a post to
the `comment-list` component:

```html
<div class="post-list d-flex flex-row flex-wrap justify-content-between">
  <div
    class="card"
    *ngFor="let p of postsArray"
    style="width: 30%; margin-bottom: 20px"
  >
    <div class="card-body">
      <h3>{{ p.title }}</h3>
      <app-comment-list [comments]="p.comments"></app-comment-list>
      <app-comment-create [postId]="p.id"></app-comment-create>
    </div>
  </div>
</div>
```

_Note: Typescript complains about the typing of the `comments` property inside
of `comment-list` component when making this change. The solution works; moving
ahead for now._

```ts
import { Component, Input } from '@angular/core';
@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css'],
})
export class CommentListComponent {
  @Input()
  comments: any[] = [];
}
```

Use the `content` property to output the comment text int the `comment-list`
template file:

```html
<ul>
  <li *ngFor="let c of comments">{{ c.content }}</li>
</ul>
```

We're now successfully communicating **across services**, and **minimizing
requests** to our backend.
