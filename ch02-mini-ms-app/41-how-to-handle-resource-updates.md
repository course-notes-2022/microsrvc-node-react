## How to Handle Resource Updates

A better solution than the two options we've seen is to have the comment
moderation be processed by the **comment service**. The comment service contains
all the business logic for processing comments. Whenever the comment service
updates a comment in a specific way, we'll have it emit a **generalized event**
to the `query` service:

```
| CommentService          |     | QueryService
| type: CommentModerated  |     |
| type: CommentUpvoted    |     |
| type: CommentDownvoted  | --> |
| type: CommentPromoted   |     | type: CommentUpdated
| type: CommentAnonymized |     |
| type: CommentSearchable |     |
| type: CommentAdvertised |     |


```

## Option 3: Comment Moderation is Processed by `CommentService`

![option 3](./option_3.png)

1. User submits comment to Comments srvc
2. Comments service persists event **along with comment status**
3. Comments service emits `CommentCreated` event to Event Bus
4. Event bus emits `CommentCreated` event to Moderation and Query services
5. Moderation moderates event and sends `CommentModerated` event to event bus
6. Event bus sends `CommentModerated` event to comments service
7. Comments service updates status of comment status and emits `CommentUpdated`
   event to event bus
8. Event bus sends `CommentUpdated` event to query service
9. Query service takes `CommentUpdated` event and **all its attributes**, and
   updates saved comment

Now, the query service needs to care only that the comment was **updated**, but
not _how_ it was updated.
