# Fetch and Render Posts

Now let's learn to fetch and render our posts.

Add the following to the `post-list` component file:

```ts
import { Component, Input, OnInit } from '@angular/core';
import { PostService } from '../post-service';
import { Post } from '../Post';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
})
export class PostListComponent implements OnInit {
  constructor(private postService: PostService) {}

  posts: { [key: string]: Post } = {};
  postsArray: Post[] = [];

  ngOnInit(): void {
    this.postService.getPosts().subscribe((data: Object) => {
      this.posts = data as { [key: string]: Post };
      this.postsArray = Object.values(this.posts);
    });
  }
}
```

Add the following to the `html` template:

```html
<div class="post-list d-flex flex-row flex-wrap justify-content-between">
  <div
    class="card"
    *ngFor="let p of postsArray"
    style="width: 30%; margin-bottom: 20px"
  >
    <div class="card-body">
      <h3>{{ p.title }}</h3>
    </div>
  </div>
</div>
```

Now, our post list should fetch all the existing posts on initialization. Save
your changes and verify that posts are now visible in the running Angular app.
