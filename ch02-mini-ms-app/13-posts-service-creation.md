# `Posts` Service Creation

Let's now begin implementing our `Posts` service. For now, we'll write just the
basic functionality for dealing with a `Post`.

## Designing our Posts Service

Our service needs to accomplish the following:

| Path     | Method | Body              | Goal               |
| -------- | ------ | ----------------- | ------------------ |
| `/posts` | `POST` | `{title: string}` | Create a new post  |
| `/posts` | `GET`  | _none_            | Retrieve all posts |

Create a new `index.js` file and add the following code:

```js
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const { randomBytes } = require('crypto');
const PORT = 4000;

const posts = {};

app.use(bodyParser.json());

app.get('/posts', (req, res) => {
  res.status(200).json(posts);
});

app.post('/posts', (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;
  posts[id] = { id, title };
  res.status(201).json(posts[id]);
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
```

Create a new `script` in `package.json`:

```json
{
  "scripts": {
    "start": "node index.js",
    "dev": "nodemon index.js"
  }
}
```

Run the `start` script in a terminal, and verify that the `posts` service starts
successfully.
