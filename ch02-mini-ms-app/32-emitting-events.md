# Emitting Events

Let's now refactor our `post` and `comments` service to send a new event to our
event bus when a post or comment is created. We will then refactor the event bus
to emit the event **back to** the other services.

Add the following handler for a `POST` request to the `post` service:

```js
const express = require('express');
const axios = require('axios');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const { randomBytes } = require('crypto');
const PORT = 4000;

const posts = {};

app.use(bodyParser.json());
app.use(cors());

app.get('/posts', (req, res) => {
  res.status(200).json(posts);
});

app.post('/posts', async (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;
  posts[id] = { id, title };
  // Make POST request to event bus
  await axios.post('http://localhost:4005/events', {
    type: 'PostCreated',
    data: { id, title },
  });
  res.status(201).json(posts[id]);
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
```

In the browser client, attempt to create a new post. Note that the request will
**fail** with a `404`. This is **expected**, as the `post` service _does not yet
have a route for `/events`_. We'll fix this in a future lesson.
