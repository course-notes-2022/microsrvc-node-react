# React Project Setup

Let's begin working on our client app. Our app will have the following
structure:

```
            App
        _____|_____
       |           |
    PostList     PostCreate
_______|_______
|             |
CommentList   CommentCreate

```

Create the components above in your (Angular) app directory. No need to add any
content just yet; we'll begin doing that next.
