# Build Deployment for Event Bus

Let's now create a `Deployment` object for the `event-bus` microservice. We'll
follow the steps below:

1. **Build an image** for the event bus

2. **Push** the image to Dockerhub

3. Create a **deployment** for the event bus

4. Create a `ClusterIP` service for `event-bus` and `posts`

5. "Wire it up" (more on this later)

## Step 1:

```
docker build -t event-bus-ms:0.0.0 -f <Dockerfile-location> .
```

## Step 2:

```
docker tag event-bus-ms:0.0.0 <your-dockerhub-username>/event-bus-ms:0.0.0


docker push <your-dockerhub-username>/event-bus-ms:0.0.0

```

## Step 3:

Add the following to your `posts-deployment` file (**or** create a new
`event-bus-deployment.yaml` file):

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: event-bus-depl
  namespace: blog-project
  labels:
    app: blog-project-event-bus
spec:
  selector:
    matchLabels:
      app: blog-project-event-bus
  replicas: 1
  template:
    metadata:
      matchLabels:
        app: blog-project-event-bus
    spec:
      containers:
        - name: event-bus
          image: jfarrow02/event-bus-ms:0.0.0

---
```

## Step 4:

> `kubectl apply -f <depl-file-location>`

Note that we cannot yet _communicate_ with the event bus deployment, because we
have not yet assigned it a `ClusterIP` service object. We'll do that next.
