# Writing Ingress Config Files

Now that we've installed `IngressController` and created an ingress controller
inside our cluster, **how do we tell it to route traffic to our pods?**

![configure ingress controller](./screenshots/config-ingress-ctrlr.png)

We will create a **config file** that contains **router rules**.
`IngressController` will auto-discover it and update its internal configuration
to match the rules in our config file.

## Config File

Create a new config file in `infra/k8s`, `ingress-srvr.yaml`:

`ingress-srvr.yaml`:

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress-srvr
  annotations:
    kubernetes.io/ingress.class: nginx # tells ingress controller that this config has routing rules
spec:
  rules:
    - host: posts.com
      http:
        paths:
          # Send any requests coming in for /posts to Posts ClusterIP service object
          - path: /posts
            backend:
              serviceName: posts-internal-srvc
              servicePort: 4000
```

Apply the config file in the `infra/k8s` directory:

> `kubectl apply -f ingress-srvr.yaml`
