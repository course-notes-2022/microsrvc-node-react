# Skaffold API version Update

The `v2Alpha3` API version of Skaffold that is used in the course is a few
versions behind. Based on all of our recent testing this should still be
supported and work without any errors or issues.

`skaffold schema list` will return the API versions that are supported by the
version of Skaffold you have installed.

That said, some students may want to upgrade their `skaffold` config. There is a
very easy way to do this by just running `skaffold fix` from your terminal:

[https://skaffold.dev/docs/references/cli/#skaffold-fix](https://skaffold.dev/docs/references/cli/#skaffold-fix)

This will print an updated version of your Skaffold config to the terminal so
that you can copy-paste or review and update as needed. This will not
automatically update or modify your existing file.

**The main difference between the two APIs is that the `deploy` and `kubectl`
fields no longer exist**:

```yaml
apiVersion: skaffold/v2alpha3
kind: Config
deploy: kubectl:
manifests:
    - ./infra/k8s/\*
```

Should now be written as:

```yaml
apiVersion: skaffold/v4beta3
kind: Config
manifests:
  rawYaml:
    - ./infra/k8s/\*
```

**See below for the full updated `skaffold.yaml` file**:

```yaml
apiVersion: skaffold/v4beta7
kind: Config
build:
  artifacts:
    - image: jfarrow02/comments-ms
      context: comments
      sync:
        manual:
          - src: '*.js'
            dest: .
      docker:
        dockerfile: Dockerfile
    - image: jfarrow02/event-bus-ms
      context: event-bus
      sync:
        manual:
          - src: '*.js'
            dest: .
      docker:
        dockerfile: Dockerfile
    - image: jfarrow02/moderation-ms
      context: moderation
      sync:
        manual:
          - src: '*.js'
            dest: .
      docker:
        dockerfile: Dockerfile
    - image: jfarrow02/posts-ms
      context: posts
      sync:
        manual:
          - src: '*.js'
            dest: .
      docker:
        dockerfile: Dockerfile
    - image: jfarrow02/query-ms
      context: query
      sync:
        manual:
          - src: '*.js'
            dest: .
      docker:
        dockerfile: Dockerfile
  local:
    push: false
manifests:
  rawYaml:
    - ./infra/k8s/*
deploy:
  kubectl: {}
```
