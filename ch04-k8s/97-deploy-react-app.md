# Deploying the React App

Now that we've configured `Ingress` for routing requests from clients to the
appropriate MS pod in our cluster, we need to refactor the frontend app to make
requests to the configured domains, i.e. `posts.com`.

`PostList.js`:

```javascript

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CommentCreate from './CommentCreate';
import CommentList from './CommentList';
import CONFIG from '../../config';

const {
  posts: { domain },
} = CONFIG;

const PostList = () => {
  const [posts, setPosts] = useState({});

  const fetchPosts = async () => {
    // Point requests to "posts.com/posts"
    const res = await axios.get(`http://${domain}/posts`);

    setPosts(res.data);
  };
```

Now, when we make a request to `posts.com` from the client app, our local OS
will **redirect it to our local machine** where it will be handled by K8s.

From here, we need to create a **Docker image** of our client app, then create a
K8s `Deployment` and `ClusterIP` so that we can host our client app **inside our
cluster** and the `ingress-nginx` controller can direct traffic to the pod and
serve the `html`, `css` and `javascript` out of it.

In the `client` directory, create a Dockerfile if you haven't already to build
an image from the client app. Build the image with `docker build` and push to
Dockerhub with `docker push`<sup>\*</sup>. Create a new `Deployment` object in
the `infra/k8s` directory:

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: client-depl
  namespace: blog-project
  labels:
    app: blog-project-client
spec:
  selector:
    matchLabels:
      app: blog-project-client
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-client
    spec:
      containers:
        - name: client
          image: jfarrow02/client:latest

---
apiVersion: v1
kind: Service
metadata:
  name: client-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-client
  ports:
    - name: client
      protocol: TCP
      port: 4200
      targetPort: 4200
```

Run `kubectl apply` again to create the deployment and service inside your K8s
cluster.

**<sup>\*</sup> Note:** Investigate the proper way to Dockerize an Angular app
and refactor this file with that information.
