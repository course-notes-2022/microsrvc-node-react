# Creating a NodePort Service

Let's learn to create a NodePort service. A LoadBalancer is _better suited for
production_, but slightly more complex to create. We'll learn to do so in a
later lesson. For now, we'll create a NodePort.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: posts-srvc
spec:
  type: NodePort
  selector:
    app: posts-depl # tell service to expose pods with label "app: posts-depl" to outside world
    ports:
      - name: posts-srvc # for logging purposes only
        protocol: TCP
        port: 4000
        targetPort: 4000
```

### `Port` and `targetPort`

![port and targetPort](./port_targetport.png)

The `port` attribute refers to the port number where the **service** is
listening.

The `targetPort` attribute refers to the port number where the **container
running the application image** is listening.

`nodePort` refers to the port from which the **request from the browser** comes
in. It **must** be a value between `30000` and `32657`.
