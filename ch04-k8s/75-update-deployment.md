# Updating Deployments

We use deployments because they allow us to easily update the version image used
by the pods inside the deployment. Let's look a how to apply those deployments.
There are 2 methods. We'll look at the first here.

1. Make a change to your **application code**

2. Rebuild the **image**, tagging it with a new version number (with
   `docker build` command)

3. Update the **image version** in the **Deployment file**

4. **Re-run** the `kubectl apply -f <depl-file-name>` command

K8s will update only the configurations that have **changed** (rather than
creating a new deployment, etc.).

_Note that this is **not** the preferred method for updating a deployment._ The
reason is that we are specifying the version **manually**. Any time the version
_changes_, we have to make a change to this file. Cumbersome!

We'll look at the **preferred** method next.
