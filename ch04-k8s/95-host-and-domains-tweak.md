# IMPORTANT: Hosts File Tweak

In K8s, we can host many different domains inside a single cluster:

![multiple domains](./screenshots/multiple-domains.png)

`Ingress` is set up expecting that you might be hosting many different apps at
different domains. The `host` attribute indicates the host/domain for **this
particular configuration**.

In the `/infra/k8s` directory, **create the following in `ingress-srvr.yaml`**:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-srvr
  namespace: blog-project
spec:
  ingressClassName: nginx
  rules:
    - host: posts.com
      http:
        paths:
          - path: /posts
            pathType: ImplementationSpecific
            backend:
              service:
                name: posts-internal-service
                port:
                  number: 4000
```

[See the K8s docs on `Ingress` at https://kubernetes.io/docs/concepts/services-networking/ingress/](https://kubernetes.io/docs/concepts/services-networking/ingress/)
for more configuration information.

**BUT**, in the development environment we typically serve all our applications
from `localhost`.

In development, we have to "trick" our computer into thinking that `posts.com`
actually points to `localhost`. To do this we make changes to our `hosts` file:

| Operating System | Host File Location                      |
| ---------------- | --------------------------------------- |
| MacOS/Linux      | `/etc/hosts`                            |
| Windows          | `C:\Windows\System32\Drivers\etc\hosts` |

**Run the following command to get the _IP address of the ingress_**:

> `kubectl get ingress -n <namespace>`

Verify that the output is similar to the following:

```
NAME           CLASS   HOSTS       ADDRESS        PORTS   AGE
ingress-srvr   nginx   posts.com   192.168.49.2   80      19m

```

Note the `ADDRESS` value

Add the following line as the last entry in your `hosts` file:

```sh
# if using MINIKUBE, add the output of the `minikube ip` command:
192.168.49.2 posts.com
```

Now whenever we attempt to connect to `posts.com`, your machine will send the
request to the address above.

In the browser, make a request to `posts.com/posts`:

1. Your OS will **re-route** the request to the `posts.com` domain to
   `localhost` (`127.0.0.1`)

2. The request will be handled by the `posts-ms` ClusterIP service.

Verify that the output is similar to the following:

```json
// 20231029184720
// http://posts.com/posts

{
  "df14111f": {
    "id": "df14111f",
    "title": "post #2"
  }
}
```
