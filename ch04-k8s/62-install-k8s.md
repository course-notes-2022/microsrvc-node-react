# Install K8s

Kubernetes (K8s) is a **container orchestration tool**.

[See here for installation instructions](https://kubernetes.io/docs/tasks/tools/)
on how to install K8s tools on your OS.
