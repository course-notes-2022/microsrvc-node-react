# Networking with Services

**Services** are K8s objects that **provide networking between pods**, or to
**access a pod outside the cluster**. We create services using config files,
just as with all other K8s objects. Think services **whenever you think of
networking/communication in K8s!**

![pod to pod](./screenshots/pod_to_pod.png)

![pod to outside](./screenshots/pod_to_outside.png)

There are **4 types** of services:

1. **Cluster IP Services**: Sets up an easy-to-remember URL to access a pod.
   Only exposes pods _in the cluster_.

2. **Node Port**: Makes a pod accessible from _outside the cluster_. Usually
   only used for dev purposes.

3. **Load Balancer**: Makes a pod accessible from _outside the cluster_. **This
   is the right way to expose a pod to the outside world**.

4. External Name: Redirects an in-cluster request to a CNAME url.

We usually use only **ClusterIP** and **LoadBalancer** on a daily basis.
