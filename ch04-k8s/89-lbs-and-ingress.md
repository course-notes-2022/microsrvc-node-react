# Load Balancers and Ingress

## Key Terminology

1. `LoadBalancer` Service: tells K8s cluster to reach out to its provider (AWS,
   GCP, Azure, etc.) and provision a **provider** load balancer. The load
   balancer lives in the cloud provider's network **outside of our cluster**.
   Gets traffic into a _single pod_.

   ![load balancer](./screenshots/loadbalancer-srvc.png)

2. `Ingress` or `IngressController`: a pod with a set of routing rules to
   distribute traffic to other **services** inside the cluster. It works
   **alongside** load balancers from the cloud provider and routes traffic to
   the correct pod's `ClusterIP` service. The React app can then contain **no
   routing logic**. It simply makes a request to a certain route and
   `IngressController` contains the logic to route the request to the correct
   pod/container.

   ![ingress controller](./screenshots/ingress-ctrlr.png)
