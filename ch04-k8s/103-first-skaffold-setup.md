# First Time Skaffold Startup

We are now ready to start `skaffold` up. Run the following command:

> `skaffold dev`

You should notice a lot of output in the terminal. `skaffold` will probably try
to build all your images again.
