# Testing Communication

Let's now update the `event-bus` to remove `localhost` API calls and refactor it
to call to our K8s cluster service objects. Make the following changes to
`event-bus/index.js`:

```js
app.post('/events', (req, res) => {
  const event = req.body;
  events.push(event);
  // Add the service names
  axios.post('http://posts-internal-service:4000/events', event);
  axios.post('http://comments-ms-internal-service:4001/events', event);
  axios.post('http://query-depl-internal-service:4002/events', event);
  axios.post('http://moderation-ms-internal-service:4003/events', event);
  res.send({ status: 'OK' });
});
```

Rebuild the `event-bus` image and push it to Dockerhub. Run the command:

> `kubectl rollout restart deployments/<event-bus-depl-name> -n <namespace>`

Create a Postman request to the `posts` external service's URL to create a new
post. Verify that the expected output is logged to the console using the
`kubectl logs` command.
