# Notes on Config Files

K8s config files tell K8s about the different Deployments, Pods, and Services
(referred to as "Objects") that we want to create. Config files are written in
YAML.

Always store these files with our prooject source code; they are documentation!

We _can_ create Ojbects _without_ config files; **do not do this**. Config files
provide a precise definitiion of what your cluster is running.
