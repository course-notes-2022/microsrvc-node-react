# Creating a Deployment

Let's create a Deployment config file for our `posts` service.

Begin by deleting the Pod deployment file in the `infra/k8s` directory. Create a
new one, `posts-depl.yaml` with the following content:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: posts-depl
spec:
  replicas: 1 # number of pod replicas to create
  selector:
    matchLabels:
      app: posts
  template:
    metadata:
      labels:
        app: posts
    spec:
      containers:
        - name: posts
          image: <your-dockerhub-id>/posts:0.0.1
```

The `selector` and `template` attrs work together to tell the deployment **which
pods to manage**:

- `selector` tells the deployment to look at all pods, and find those pods with
  the **label** `app: posts`.

- `template` specifies the configuration of the **pods** we want the deployment
  to create. It instructs the deployment to create pod(s) with a label of
  `app: posts`.

This is a basic, **working example** of a deployment config file. Run the file
with `kubectl apply -f posts-depl.yaml`.
