# How to Communicate Between Services

It's now time for us to learn what step 5 ("Wire it all up") really means!

Currently the `Posts` service in our source code communicates with the
`EventBus` service at `localhost`:

```javascript
// ...

app.post('/posts', async (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;

  posts[id] = {
    id,
    title,
  };

  await axios.post('http://localhost:4005/events', {
    type: 'PostCreated',
    data: {
      id,
      title,
    },
  });

// ...
```

`localhost` will **not** work **inside our cluster**. We need to find a way to
have our services communicate with one another **via the CluserIP services** we
assigned them in K8s:

![communicating between services](./comm_betw_srvcs.png)

**We need to use the NAME of the event bus service** in our URL!

![use event bus name](./use_eventbus_name.png)

```javascript
// ...

app.post('/posts', async (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;

  posts[id] = {
    id,
    title,
  };

// Replace 'localhost' with `name` attribute of event bus service
  await axios.post('http://event-bus-srvc:4005/events', {
    type: 'PostCreated',
    data: {
      id,
      title,
    },
  });

// ...
```
