# Load Balancer Services: Integrating the React App

The React app will be housed inside a **pod** that will exist inside our K8s
cluster. How do we make sure that the React app running in the browser can make
requests to all the service pods?

## Load Balancer Service

Note that once the (React) frontend app is running in the browser, **all
requests to the service pods come directly from the browser**, NOT the pod
serving the frontend app:

![react-ms app structure](./screenshots/react-ms-app-structure.png)

We have several options for **routing the request(s)** from the **browser** to
the services **inside the cluster**.

## Option 1: BAD Idea

![option 1](./screenshots/lb-option-1.png)

Create a `NodePort` service in front of each MS pod that exposes the pod to the
outside world. Recall that `NodePort` exposes a port that we **cannot know
beforehand**. If the port ever _changes_, _we have to **update and re-deploy**
our client app_!

## Option 2: GOOD Idea

![option 2](./screenshots/lb-option-2.png)

Create a `LoadBalancer` service that provides a **single point of entry** to the
cluster. Logic inside the load balancer routes traffic **from** the React app
**to** the `ClusterIP` services of the appropriate ms pod.
