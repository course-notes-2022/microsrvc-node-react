# Unique Route Paths

Let's set up routing rules for the rest of our microservices in our ingress
configuration file. Right now we have a `rule` for the `posts` ms _only_. We
need to add paths for **all** the routes we need to serve in the
`ingress-config` file:

![configure remaining routes](./screenshots/configure-remaining-routes.png)

**However, there's a problem**. `nginx` can route requests according to the
**route only**, _not_ the request method. We have **two routes** that point
towards the `/posts` endpoint: one for `posts` ms and another for `query`. How
do we handle this in our configuration?

The solution is that we need to ensure that **all paths in our application are
_unique_**:

![unique paths](./screenshots/unique-paths.png)

To do this, we need to make sure we update the **routes** in our microservices,
as well as the **client** calls to our services:

`posts/index.js`:

```js
// Add `/create` to the path
app.post('/posts/create', async (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;
  posts[id] = { id, title };
  await axios.post('http://event-bus-internal-service:4005/events', {
    type: 'PostCreated',
    data: { id, title },
  });
  res.status(201).json(posts[id]);
});
```

`client/posts-service.ts`:

```ts
// Add `/create` to the path
createPost(post: Post): Observable<Post> {
    return this.client.post<Post>('http://posts.com:4000/posts/create', post);
  }
```

Rebuild the `client` and `posts` images and push them up to dockerhub. Run
`kubectl rollout restart deployment <deployment-name>`. **Note: Don't forget to
push the `latest` tag version of the deployments!**

Send a `POST` request to `http://posts.com/posts/create` and verify that the
response is `201`:

![unique route path success](./screenshots/unique-route-path-success.png)
