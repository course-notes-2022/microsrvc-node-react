# Important Note About Port 80

In the upcoming lecture, we will be editing our hosts file so that we can access
posts.com/posts in our browser. If you are unable to access the application you
may have something already running on port 80, which is the default port for the
ingress. Before doing anything, please make sure you have properly installed the
ingress-nginx controller for your particular Kubernetes client. Many students
are skipping this step!

[https://www.udemy.com/course/microservices-with-node-js-and-react/learn/lecture/26492690#questions](https://www.udemy.com/course/microservices-with-node-js-and-react/learn/lecture/26492690#questions)

Once you have confirmed that you have indeed installed/enabled the ingress-nginx
controller, you'll need to identify if something is running on port 80 and shut
it down. Some students have even had applications from other courses or personal
projects still running. For Windows Pro users, both SQL Server Reporting
Services (MSSQLSERVER) and the World Wide Web Publishing Service / IIS Server
have been the most common services causing a conflict.
