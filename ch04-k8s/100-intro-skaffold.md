# Introducing Skaffold

We've gotten things working pretty well. However, the process of **updating our
application code**, **rebuilding and pusing new images to Dockerhub**, and
**restarting our deployments** is quite cumbersome. It's still important to
understand how to do this for _production_, but is there a better way in
_development_?

## Skaffold to the Rescue

**Skaffold** is a tool that **automates many tasks in a K8s dev environment.**
Skaffold makes it easy to update code in a _running_ K8s pod, as well as
create/delete all objects tied to a specific project at once.

Documentation and installation instructions at
[skaffold.dev](https://skaffold.dev/)

```sh
# For Linux x86_64 (amd64)
curl -Lo skaffold https://storage.googleapis.com/skaffold/releases/v2.0.0/skaffold-linux-amd64 && \
sudo install skaffold /usr/local/bin/
```
