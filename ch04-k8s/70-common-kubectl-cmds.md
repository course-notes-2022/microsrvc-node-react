# Common `kubectl` Commands

- `kubectl get pods`: list information about the running pods

- `kubectl exec -it <pod-name> <cmd>`: execute the given command in a running
  pod

- `kubectl logs <pod-name>`: print out logs from a given pod

- `kubectl delete pod <pod-name>`: manually delete a pod

- `kubectl apply -f <config-file-name>`: execute the config file given to create
  Objects

- `kubectl describe pod <pod-name>`: prints out information about a pod,
  including **events log** with debug information
