# Final Route Config

Let's complete our refactor of the `ingress-controller` config with our new
routes.

Make the following changes to `ingress-srvr.yaml`:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-srvr
  namespace: blog-project
  annotations:
    nginx.ingress.kubernetes.io/use-regex: 'true'
spec:
  ingressClassName: nginx
  rules:
    - host: posts.com
      http:
        paths:
          - path: /posts/create
            pathType: ImplementationSpecific
            backend:
              service:
                name: posts-internal-service
                port:
                  number: 4000
          - path: /posts
            pathType: ImplementationSpecific
            backend:
              service:
                name: query-depl-internal-service
                port:
                  number: 4002
          - path: /posts/?(.*)/comments # Regular expression/wildcard will match `/posts/<anything>/comments`
            # https://kubernetes.github.io/ingress-nginx/user-guide/ingress-path-matching/
            pathType: ImplementationSpecific
            backend:
              service:
                name: comments-ms-internal-service
                port:
                  number: 4001

          # NOTE: commenting out client config until Angular application is configured
          # - path: /?(.*)
          #   pathType: ImplementationSpecific
          #   backend:
          #    service:
          #     name: client-service
          #     port:
          #      number: 4200
```
