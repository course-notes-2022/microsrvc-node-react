# Setting Up a Cluster IP Service

A **ClusterIP** service exposes a pod to **other pods in the cluster**.

In this module we will create a `Deployment` for `EventBus` service, and create
`ClusterIP` service objects to allow **communication between `EventBus` and
`Posts`**.

![cluster ip communication between pods](./screenshots/cluster_ip_communication.png)

Here we have two pods; one running `posts` and another running the `event-bus`
containers. Technically, the pods _can_ communicate _directly_ with one another,
but we _don't want them to do so_, for a couple of reasons:

1. We have no way to know ahead of time what the IP address of a new pod will be

Instead, we create a `ClusterIp` service for each pod, that allows communication
**to** that pod **from** other pods.
