# Adding More Services

For the `Comments`, `Query`, and `Moderation` services, we can now containerize
and manage them with K8s just like we did with the `EventBus` and `Posts`
services:

1. Update the URLs in each to reach out to the `event-bus-srvc` service object

2. Build images and push to DockerHub

3. Create a deployment and ClusterIp service for each of them

4. Update the `EventBus` to once again send events to the remaining services

## Full Deployment File

`posts-depl.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: posts-depl
  namespace: blog-project
  labels:
    app: blog-project-posts
spec:
  selector:
    matchLabels:
      app: blog-project-posts
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-posts
    spec:
      containers:
        - name: posts
          image: jfarrow02/posts-ms

---
apiVersion: v1
kind: Service
metadata:
  name: posts-service
  namespace: blog-project
spec:
  type: NodePort
  selector:
    app: blog-project-posts
  ports:
    - name: posts
      protocol: TCP
      port: 4000 # SERVICE listens HERE
      targetPort: 4000 # CONTAINER listens HERE

---
apiVersion: v1
kind: Service
metadata:
  name: posts-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-posts
  ports:
    - name: posts
      protocol: TCP
      port: 4000
      targetPort: 4000

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: event-bus-depl
  namespace: blog-project
  labels:
    app: blog-project-event-bus
spec:
  selector:
    matchLabels:
      app: blog-project-event-bus
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-event-bus
    spec:
      containers:
        - name: event-bus
          image: jfarrow02/event-bus-ms:latest

---
apiVersion: v1
kind: Service
metadata:
  name: event-bus-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-event-bus
  ports:
    - name: event-bus
      protocol: TCP
      port: 4005
      targetPort: 4005

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: comments-depl
  namespace: blog-project
  labels:
    app: blog-project-comments-depl
spec:
  selector:
    matchLabels:
      app: blog-project-comments-depl
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-comments-depl
    spec:
      containers:
        - name: comments-ms
          image: jfarrow02/comments-ms:latest

---
apiVersion: v1
kind: Service
metadata:
  name: comments-ms-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-comments-depl
  ports:
    - name: comments
      port: 4001
      targetPort: 4001
      protocol: TCP

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: query-depl
  namespace: blog-project
  labels:
    app: blog-project-query-depl
spec:
  selector:
    matchLabels:
      app: blog-project-query-depl
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-query-depl
    spec:
      containers:
        - name: query-ms
          image: jfarrow02/query-ms:latest

---
apiVersion: v1
kind: Service
metadata:
  name: query-depl-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-query-depl
  ports:
    - name: query
      protocol: TCP
      port: 4002
      targetPort: 4002
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: moderation-depl
  namespace: blog-project
  labels:
    app: blog-project-moderation-depl
spec:
  replicas: 1
  selector:
    matchLabels:
      app: blog-project-moderation-depl
  template:
    metadata:
      labels:
        app: blog-project-moderation-depl
    spec:
      containers:
        - name: blog-project-moderation-depl
          image: jfarrow02/moderation-ms:latest

---
apiVersion: v1
kind: Service
metadata:
  name: moderation-ms-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-moderation-depl
  ports:
    - name: moderation
      protocol: TCP
      port: 4003
      targetPort: 4003
```
