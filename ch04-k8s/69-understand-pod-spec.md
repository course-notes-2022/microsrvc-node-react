# Understanding a Pod Spec

Let's understand the pod spec that we just wrote:

```yaml
apiVersion: v1 # Specify the set of objects we want k8s to look at
kind: Pod # type of object we want to create
metadata: # config options for the object
  name: posts # give pod the name "posts"
spec: # exact attrs we want to apply to the object
  containers: # an array of containers we want in our pod
    - name: posts # container name will be "posts"
      image: <your-dockerhub-repo>/posts:0.0.1 # image we want to use
```
