# A Time-Saving Alias

You can create an **alias** for the `kubectl` command that can save you some
typing. How you do this varies based on your shell of choice. You'll need to do
some research for your particular setup.

In `zshell`, simply add a line `alias k="kubectl"` in the `~/.zshrc` file.
