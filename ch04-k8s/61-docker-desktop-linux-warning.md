# Warning on Docker Desktop for Linux

In the upcoming lecture, we will be discussing the installation of Kubernetes.
This note is a warning for students who have natively installed a Linux OS and
are attempting to use Docker Desktop. This does not pertain to students running
Linux with Windows WSL.

Docker Desktop for Linux is just out of beta and has proven to be very buggy. It
appears that it is also having issues working at all with Skaffold and syncing.

For these reasons, we do not recommend using it until it is more stable.
Minikube has been the standard solution for running Kubernetes on Linux for many
years, so, that would be our recommendation for this course.
