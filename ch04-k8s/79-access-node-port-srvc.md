# Accessing the NodePort Services

## Start the Service

Apply the service configuration file using `kubectl apply -f {file-name}` as
usual to start the service.

## Access the Service from the Browser: LINUX

If you are running `minikube` on Linux, **run the following command to get the
service IP address**:

- `minikube ip`

Run the following command to get the `nodePort` (`3XXXX`) you can use in the
**browser bar** to access the service:

- `kubectl describe service {service-name}`

This returns a configuration with the `nodePort` attribute.

You can now access the service at the `http://{ip-address}:{node-port}/posts`
location:

![node port in browser](./screenshots/access-nodeport-in-browser.png)

## Access the Service from the Browser: MacOS/Windows

You can simply access the service at `localhost:3XXXX`

![access service](./access_service.png)
