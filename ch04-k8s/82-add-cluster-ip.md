# Adding a `ClusterIp` Service to `event-bus`

Add the following to your deployment file for your `posts` and `event-bus`
microservices:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: posts-depl
  namespace: blog-project
  labels:
    app: blog-project-posts
spec:
  selector:
    matchLabels:
      app: blog-project-posts
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-posts
    spec:
      containers:
        - name: posts
          image: jfarrow02/posts-ms:0.0.0

---
apiVersion: v1
kind: Service
metadata:
  name: posts-service
  namespace: blog-project
spec:
  type: NodePort
  selector:
    app: blog-project-posts
  ports:
    - name: posts
      protocol: TCP
      port: 4000 # SERVICE listens HERE
      targetPort: 4000 # CONTAINER listens HERE

---
apiVersion: v1
kind: Service
metadata:
  name: posts-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-posts
  ports:
    - name: posts
      protocol: TCP
      port: 4000
      targetPort: 4000

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: event-bus-depl
  namespace: blog-project
  labels:
    app: blog-project-event-bus
spec:
  selector:
    matchLabels:
      app: blog-project-event-bus
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-event-bus
    spec:
      containers:
        - name: event-bus
          image: jfarrow02/event-bus-ms:0.0.0

---
apiVersion: v1
kind: Service
metadata:
  name: event-bus-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-event-bus
  ports:
    - name: event-bus
      protocol: TCP
      port: 4005
      targetPort: 4005
```

**Note the following**:

1. We are creating **two** `ClusterIp` services: **one** for the `posts` ms, and
   **another** for the `event-bus`. This is to allow **internal communication**
   between the services.

2. The `metadata.name` attributes for the `posts` ClusterIp and NodePort
   services **must** be different.

Run `kubectl apply` again to create/update your objects.
