# Tour of Kubernetes

Run the following command to verify K8s is working in a **new** terminal window:

> `kubectl version`

## Dockerize Application Files

In this part of a K8s workflow, we create docker images for our application(s):

![dockerize app files](./screenshots/dockerize-app-files.png)

We can use the image to create **containers**.

## Deploy Container to Cluster

We deploy containers inside **K8s clusters**. A cluster contains one **master**
node and one or more **worker** nodes, in which we deploy our containers to
**pods**.

![deploy to cluster](./screenshots/deploy-to-cluster-2 .png)

We use **configuration files** to tell K8s what containers to create, and how to
communicate between/among containers.

We use the `kubectl` command line utility to interact with our cluster and issue
commands to it.

### Deployments

K8s uses **deployments** to manage the pods we create. If the pod crashes, the
deployment will ensure that new pod(s) are created to replace those that crash.

### Services

A **service** in K8s is like a durable IP address that allows us to access
running containers inside pods. Pods are **ephemeral**; they can be
destroyed/created at any time. Services allow us to have a durable,
easy-to-understand IP for our ephemeral pods.
