# `Skaffold` Setup Notes

Setting up `skaffold` successfully required a bit of additional research and
configuration than was shown in the video tutorials. This document contains
instructions on the **most recent requirements as of this writing**, with a
**working sample `nodejs` API app** using `skaffold`.

## Install `buildx-plugin`

`buildx` is a Docker CLI plubin for extended build capabilities with `BuildKit`.
It's necessary to install becase the familiar `docker build` command will soon
be deprecated in favor of `buildx`.

To install the `buildx-plugin`:

1. Download the
   [relevant binary for your operating system architecture.](https://github.com/docker/buildx/releases/latest).
   To discover your OS architecture on a Linux machine, run:

> `uname -m`

2. Rename the binary and place the renamed binary in the appropriate location
   for your OS below (create the `cli-plugins` directory if necessary): to
   install **system-wide**:

| OS          | Binary name     | Destination dir                     |
| ----------- | --------------- | ----------------------------------- |
| Linux/MacOS | `docker-buildx` | `/usr/local/lib/docker/cli-plugins` |

You may also need to run the following command to make the binary
**executable**:

> `chmod +x ~/.docker/cli-plugins/docker-buildx`

You can now run `docker buildx build` in place of the `docker build` command to
build an image.

[Docker `buildx-plugin` documentation on Github](https://github.com/docker/buildx#manual-download)

## Initialize a `skaffold` Project in your Project Root

Installing `skaffold` via the command given in `101-skaffold-setup.md` works:

> curl -Lo skaffold
> https://storage.googleapis.com/skaffold/releases/v2.0.0/skaffold-linux-amd64
> && \
> sudo install skaffold /usr/local/bin/

Creating the `skaffold.yaml` manifest _may_ work, but an easier alternative
seems to be to
[initialize per the directions in the skaffold docs](https://skaffold.dev/docs/quickstart/):

1. Run the following command:

   > `skaffold init`

2. When prompted to choose the builder, press enter to accept the default
   selection.

3. When asked which builders you would like to create Kubernetes resources for,
   press enter to accept the default selection.

4. When asked if you want to write this configuration to skaffold.yaml, type `y`
   for yes.

5. Open your new `skaffold.yaml` file. All of your Skaffold configuration lives
   in this file. We will go into more detail about how it works in later steps.

### Using Skaffold for Continuous Development

1. Start minikube:

> `minikube start --profile custom skaffold config set --global local-cluster true eval $(minikube -p custom docker-env)`

2. Run the following command to begin using Skaffold for continuous development:

> `skaffold dev`

You should see output similar to the following:

![skaffold dev](../screenshots/skaffold-dev-runs-ok.png)

3. In a **separate terminal window**, run:

> `minikube tunnel -p custom`

You should see output similar to the following:

```
Status:
	machine: custom
	pid: 303428
	route: 10.96.0.0/12 -> XXX.XXX.XX.X
	minikube: Running
	services: []
    errors:
		minikube: no errors
		router: no errors
		loadbalancer emulator: no errors


```

### Running the App and Sending an API Request

Note that the application is accessible from the outside world via a `NodePort`
service. As usual, `minikube` users must **start the service** with the
following command:

> `minikube service <service-name>`

You should see output similar to the following in the terminal:

```
|---------------|-------------------------|----------------|---------------------------|
|   NAMESPACE   |          NAME           |  TARGET PORT   |            URL            |
|---------------|-------------------------|----------------|---------------------------|
| skaffold-test | customers-external-srvc | customers/8000 | http://192.XXX.XX.X:32334 |
|---------------|-------------------------|----------------|---------------------------|
🎉  Opening service skaffold-test/customers-external-srvc in default browser...
```

**At this point, you should be able to send a client request to your API**.
Verify the IP address of your `minikube` cluster with:

> `minikube ip`

You should see output similar to `192.XXX.XX.X`.

Open up Postman, and send a `GET` request to
`http://192.XXX.XX.X:323334/customers`. You should receive a `200` response with
the following response body:

```json
[
  {
    "firstName": "Johnny",
    "lastName": "Bravo"
  }
]
```

Make the following change to the `customers` array in `index.js`:

```js
const customers = [
  {
    firstName: 'Johnny',
    lastName: 'Bravo',
    username: 'blue_suede_shoes123', // Add a property to the customer data
  },
];
```

**`skaffold` will detect the changes, rebuild the image, and update the running
K8s cluster**. You should see output in the terminal similar to the following:

```
Build [jfarrow02/customers-ms] succeeded
Tags used in deployment:
 - jfarrow02/customers-ms -> jfarrow02/customers-ms:00ebd8ec1baa839795eb46bec85ac61ca75746d4b40c97d62c3d517583fca68d
Starting deploy...
 - deployment.apps/customers-ms configured
Waiting for deployments to stabilize...
 - deployment/customers-ms is ready.
Deployments stabilized in 2.054 seconds
```

Now make the same `GET` request. Note that our response body has now
**changed**:

```json
[
  {
    "firstName": "Johnny",
    "lastName": "Bravo",
    "username": "blue_suede_shoes123"
  }
]
```

![new customer response](../screenshots/new-customer-response.png)

_**`skaffold` is now live-updating our running K8s cluster!**_

To stop the cluster and delete all objects, simply press `CTRL + C`.
