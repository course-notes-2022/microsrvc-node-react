const express = require('express');
const app = express();
const cors = require('cors');
const PORT = 8000;

const customers = [
  {
    firstName: 'Johnny',
    lastName: 'Bravo',
    username: 'blue_suede_shoes123',
  },
];

app.use(cors());

app.get('/customers', (req, res) => {
  res.status(200).send(customers);
});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
