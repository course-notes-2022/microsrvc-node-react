# Common Commands Around Deployments

- `kubectl get deployments`: list running deployments in default namespace

- `kubectl describe deployment <depl-name>`: print out details about a specific
  deployment

- `kubectl apply -f <config-file-name>`: create a deployment out of a config
  file

- `kubectl delete deployment <depl-name>`: delete a deployment
