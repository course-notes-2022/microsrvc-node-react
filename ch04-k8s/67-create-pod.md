# Creating a Pod

Let's create our first config file. This file will create a **single pod
directly**.

Inside the `posts` directory, rebuild your Docker image:

> `docker build -t <your-dockerhub-repo>/posts:0.0.1`

Create a folder structure in your project root, `infra/k8s`. Inside, create a
new file, `posts.yaml`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: posts
spec:
  containers:
    - name: posts
      image: <your-dockerhub-repo>/posts:0.0.1
```

Run the following `kubectl` command from the `k8s` directory to tell K8s to
execute our config file:

> `kubectl apply -f posts.yaml`

We have a variety of commands that allow us to inspect the state of our cluster.
Run:

> `kubectl get pods`

to get a list of all the pods in the **default** namespace.
