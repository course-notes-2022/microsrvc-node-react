# Important K8s Terminology

1. `Cluster`: a collection of nodes pulus a master node to manage them
2. `Node`: A VM that will run our containers
3. `Pod`: roughly a running container. Technically, a pod can run multiple
   containers (we won't do this).
4. `Deployment`: monitors a set of pods, and makes ure they are running and
   restarts them if they crash
5. `Service`: provides an easy-to-remember URL to access a running contianer
