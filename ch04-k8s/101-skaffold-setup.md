## Setup Skaffold

Run the following command to ensure that Skaffold is installed:

> `skaffold`:

You should see `skaffold help` output similar to any Linux `man` page.

Create a `skaffold.yaml` file inside your project. This file will tell Skaffold
how to manage the projects in your cluster. This config will be consumed by
skaffold only, and runs outside the cluster.

`skaffold.yaml`

```yaml
apiVersion: skaffold/v2alpha3
kind: Config
deploy:
  kubectl:
    manifests:
      - ./infra/k8s/*
      # location of the K8s config files that you want skaffold to watch. Skaffold will automatically `kubectl apply` those files in K8s every time a file is created or changed. Skaffold will delete all the objects related to the config files on stoppage of Skaffold.
build:
  local:
    push: false # Tells skaffold NOT to push changes to dockerhub
  artifacts:
    # 'artifacts' tells Skaffold that a pod is running     code out of the directory indicated by 'context'.   Whenver something changes in 'context' matched by the     'src' attr, take those changes and directly copy them   into pod. If we change anything in `context` that is      NOT matched by `src`, skaffold will rebuild the ENTIRE   IMAGE.
    - image: jfarrow02/client
      context: client
      docker:
        dockerfile: Dockerfile
      sync:
        manual:
          - src: 'src/**/*.js'
            dest: .

    - image: jfarrow02/comments-ms
      context: comments
      docker:
        dockerfile: Dockerfile
      sync:
        manual:
          - src: '*.js'
            dest: .

    - image: jfarrow02/event-bus
      context: event-bus
      docker:
        dockerfile: Dockerfile
      sync:
        manual:
          - src: '*.js'
            dest: .

    - image: jfarrow02/moderation-ms
      context: moderation
      docker:
        dockerfile: Dockerfile
      sync:
        manual:
          - src: '*.js'
            dest: .

    - image: jfarrow02/posts-ms
      context: posts
      docker:
        dockerfile: Dockerfile
      sync:
        manual:
          - src: 'src/**/*.js'
            dest: .

    - image: jfarrow02/query-ms
      context: query
      docker:
        dockerfile: Dockerfile
      sync:
        manual:
          - src: '*.js'
            dest: .
```

## IMPORTANT!!!

**Note: The `apiVersion` of `skaffold` has _changed_ multiple times** since the
recording of the video. The above configuration has been replaced by a **newer
version**. **See the next lesson for required updated to your `skaffold.yaml`
file!**
