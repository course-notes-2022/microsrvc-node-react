# Preferred Method for Updating Image Used by a Deployment

1. Use the `latest` tag in the deployment file pod `spec` section. Run
   `kubectl apply -f {depl-file-name}`

2. Make an update to your code

3. Build the image

4. Push the image to DockerHub

5. Run the command `kubectl rollout restart deployment {depl-name}`
