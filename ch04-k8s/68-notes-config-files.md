# Kubernetes Config Files

Config files tell K8s about the different Deployments, Pods, and Services
(reffered to as **Objects**) that we want to create. Config files are written in
YAML syntax.

Alwasy store config files with your project code; they are **documentation**!
You _can_ create objects without config files, but it is not a best practice.
Only do this for testing/learning purposes!
