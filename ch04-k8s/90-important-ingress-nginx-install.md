# IMPORTANT: Ingress Nginx Installation Info

In the upcoming lecture, we will be installing Ingress Nginx. In the video, it
is shown that there is a required mandatory command that needed to be run for
all providers. This has since been removed, so, the provider-specific commands
(Docker Desktop, Minikube, etc) are all that is required. Many students are
incorrectly installing the wrong library and are meeting errors and issues.
Please triple-check that you are installing Ingress Nginx and not Nginx Ingress,
which is a totally different and incompatible library.

## Installation - Minikube (Linux)

To **enable** the `ingress` addon in `minikube` running on Linux, run the
following command:

![install ingress-nginx](./screenshots/install-ingress-nginx.png)

> `minikube addons enable ingress`

You should see output similar to the following:

```
💡  ingress is an addon maintained by Kubernetes. For any concerns contact minikube on GitHub.
You can view the list of minikube maintainers at: https://github.com/kubernetes/minikube/blob/master/OWNERS
    ▪ Using image registry.k8s.io/ingress-nginx/controller:v1.8.1
    ▪ Using image registry.k8s.io/ingress-nginx/kube-webhook-certgen:v20230407
    ▪ Using image registry.k8s.io/ingress-nginx/kube-webhook-certgen:v20230407
🔎  Verifying ingress addon...
🌟  The 'ingress' addon is enabled

```

[https://kubernetes.github.io/ingress-nginx/deploy/#minikube](https://kubernetes.github.io/ingress-nginx/deploy/#minikube)
