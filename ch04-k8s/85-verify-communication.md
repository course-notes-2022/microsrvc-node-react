# Verifying Communication Between Pods

If we've set up everything correctly, we should now be able to make a `POST`
request to `posts` and attempt to create a new post. Recall that `posts` then
emits an event to the `event-bus`, which in turn emits the same event back to
`posts`.

Run the `kubectl get pod -n <namespace>` command to get a list of the pods in
your namespace. Run `kubectl logs <pod-name> -n <namespace>` using the name of
the `posts` pod. Verify that the output is similar to the following:

```
> posts@1.0.0 start
> node index.js

App listening on port 4000
received event: PostCreated

```

We have successfully created communication between two pods in a K8s cluster
using `ClusterIp` services, and communication with a pod from **outside** the
cluster using `NodePort` service!

## Files

`posts-depl.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: posts-depl
  namespace: blog-project
  labels:
    app: blog-project-posts
spec:
  selector:
    matchLabels:
      app: blog-project-posts
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-posts
    spec:
      containers:
        - name: posts
          image: jfarrow02/posts-ms # NOTE: removes version number to pull :latest

---
apiVersion: v1
kind: Service
metadata:
  name: posts-service
  namespace: blog-project
spec:
  type: NodePort
  selector:
    app: blog-project-posts
  ports:
    - name: posts
      protocol: TCP
      port: 4000 # SERVICE listens HERE
      targetPort: 4000 # CONTAINER listens HERE

---
apiVersion: v1
kind: Service
metadata:
  name: posts-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-posts
  ports:
    - name: posts
      protocol: TCP
      port: 4000
      targetPort: 4000

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: event-bus-depl
  namespace: blog-project
  labels:
    app: blog-project-event-bus
spec:
  selector:
    matchLabels:
      app: blog-project-event-bus
  replicas: 1
  template:
    metadata:
      labels:
        app: blog-project-event-bus
    spec:
      containers:
        - name: event-bus
          image: jfarrow02/event-bus-ms

---
apiVersion: v1
kind: Service
metadata:
  name: event-bus-internal-service
  namespace: blog-project
spec:
  type: ClusterIP
  selector:
    app: blog-project-event-bus
  ports:
    - name: event-bus
      protocol: TCP
      port: 4005
      targetPort: 4005
```

`posts/index.yaml`:

```js
const express = require('express');
const axios = require('axios');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const { randomBytes } = require('crypto');
const PORT = 4000;

const posts = {};

app.use(bodyParser.json());
app.use(cors());

app.get('/posts', (req, res) => {
  res.status(200).json(posts);
});

app.post('/posts', async (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;
  posts[id] = { id, title };
  await axios.post('http://event-bus-internal-service:4005/events', {
    type: 'PostCreated',
    data: { id, title },
  });
  res.status(201).json(posts[id]);
});

app.post('/events', (req, res) => {
  console.log(`received event: ${req.body.type}`);
  res.status(201).json({ received: true });
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
```

`event-bus/index.js`:

```js
const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cors = require('cors');

const app = express();
const PORT = 4005;

app.use(bodyParser.json());
app.use(cors());

const events = [];
app.post('/events', (req, res) => {
  const event = req.body;
  events.push(event);
  axios.post('http://posts-internal-service:4000/events', event);
  // axios.post('http://localhost:4001/events', event);
  // axios.post('http://localhost:4002/events', event);
  // axios.post('http://localhost:4003/events', event);
  res.send({ status: 'OK' });
});

app.get('/events', (req, res) => {
  res.status(200).json(events);
});

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
```
