# Updating Service Addresses

Let's now update the application code files with the appropriate
**`metadata.name`** attribute of the **`ClusterIp`** object everywhere we're
making API calls to `posts` and `event-bus`.

Recall that you can get the service names by running `kubectl get services`
command in the terminal.

Make the following change in `posts.index.js`:

```js
app.post('/posts', async (req, res) => {
  const id = randomBytes(4).toString('hex');
  const { title } = req.body;
  posts[id] = { id, title };

  // Add ClusterIp service name for event-bus
  await axios.post('http://event-bus-internal-service:4005/events', {
    type: 'PostCreated',
    data: { id, title },
  });
  res.status(201).json(posts[id]);
});
```

Make the following change in `event-bus/index.js`:

```js
app.post('/events', (req, res) => {
  const event = req.body;
  events.push(event);

  // Add Cluster IP service name for posts
  axios.post('http://posts-internal-service:4000/events', event);
  //   axios.post('http://localhost:4001/events', event);
  //   axios.post('http://localhost:4002/events', event);
  //   axios.post('http://localhost:4003/events', event);
  res.send({ status: 'OK' });
});
```

Rebuild the images for `posts` and `event-bus`, and push the tagged images to
Dockerhub.

Run the command `kubectl rollout restart deployment {depl-name}` to **update the
images inside the deployment**.

_Note: To restart deployments in a **specific namespace**, run_:

> `kubectl rollout restart deployments/<depl-name> -n <namespace>`

Run the `kubectl get pods` command to verify that your pods have been created
and are healthy.

## Test it Out in Postman!

We are ready to test our service communication in Postman. Issue a `POST`
request to the `post` service to create a new post. The `url` **must** be the
**output of the `minikube ip` command** (LINUX). The `port` **must** be the
**`port`** attribute of the `NodePort` object for the `posts` service.

**IMPORTANT**: **_Minikube users_** must run the following command to **assign
the external IP address** to the `post` service in order for the service to
work:

> `minikube service <service-name>`

Verify that the `POST` operation succeeds in Postman:

![k8s post success](./screenshots/k8s-post-success.png)
