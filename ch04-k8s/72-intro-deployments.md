# Introducing Deployments

We normally don't create Pods in isolated config files. We usually create them
as part of **Deployments**, a K8s object intended to manage a **set of identical
pods**. The Deployment:

- Restarts crashed pods when the number of running pods drops below our
  specified number

- Automatically switches to new pods when we deploy new versions of our image to
  a pod
