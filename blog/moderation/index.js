const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const CONFIG = require('./config');

const {
  eventBus: { internalHost, port: eventBusPort },
  moderation: { port },
} = CONFIG;

const app = express();
app.use(bodyParser.json());

app.post('/events', async (req, res) => {
  const { type, data } = req.body;

  if (type === 'CommentCreated') {
    const status = data.content.includes('orange') ? 'rejected' : 'approved';

    await axios.post(`http://${internalHost}:${eventBusPort}/events`, {
      type: 'CommentModerated',
      data: {
        id: data.id,
        postId: data.postId,
        status,
        content: data.content,
      },
    });
  }

  res.send({});
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
