const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const CONFIG = require('./config');
const {
  eventBus: { port: eventBusPort },
  posts: { internalHost: postsInternalHost, port: postsSrvcPort },
  comments: { internalHost: commentsInternalHost, port: commentsSrvcPort },
  query: { internalHost: queryInternalHost, port: querySrvcPort },
  moderation: {
    internalHost: moderationInternalHost,
    port: moderationSrvcPort,
  },
} = CONFIG;

const app = express();
app.use(bodyParser.json());

const events = [];

app.post('/events', (req, res) => {
  const event = req.body;

  events.push(event);

  // Posts service
  axios
    .post(`http://${postsInternalHost}:${postsSrvcPort}/events`, event)
    .catch((err) => {
      console.log(err.message);
    });

  // Comments service
  axios
    .post(`http://${commentsInternalHost}:${commentsSrvcPort}/events`, event)
    .catch((err) => {
      console.log(err.message);
    });

  // Query service
  axios
    .post(`http://${queryInternalHost}:${querySrvcPort}/events`, event)
    .catch((err) => {
      console.log(err.message);
    });

  // Moderation service
  axios
    .post(
      `http://${moderationInternalHost}:${moderationSrvcPort}/events`,
      event
    )
    .catch((err) => {
      console.log(err.message);
    });

  res.send({ status: 'OK' });
});

app.get('/events', (req, res) => {
  res.send(events);
});

app.listen(eventBusPort, () => {
  console.log(`Server listening on port ${eventBusPort}`);
});
