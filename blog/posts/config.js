const SRVC_CONFIG = {
  comments: {
    port: 4001,
    internalHost: 'comments-srvc',
    domain: 'comments.com',
  },
  eventBus: {
    port: 4005,
    internalHost: 'event-bus-srvc',
  },
  moderation: {
    port: 4003,
    internalHost: 'moderation-srvc',
    domain: 'moderation.com',
  },
  posts: {
    port: 4000,
    externalHost: 'posts-srvc',
    internalHost: 'posts-internal-srvc',
    domain: 'posts.com',
  },
  query: {
    port: 4002,
    internalHost: 'query-srvc',
    domain: 'query.com',
  },
};

module.exports = SRVC_CONFIG;
