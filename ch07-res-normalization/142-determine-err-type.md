# Determining Error Type

Let's use our new custom errors in our error-handling middleware function. Add
the following to `errorHandler`:

`src/middleware/error-handler.ts`:

```javascript
import { Request, Response, NextFunction } from 'express';
import { RequestValidationError } from '../errors/request-validation-error';
import { DatabaseConnectionError } from '../errors/database-connection-error';

export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Add error type checks
  if (err instanceof RequestValidationError) {
    console.log('handling this error as a request validation error');
  }

  if (err instanceof DatabaseConnectionError) {
    console.log('handling this error as a database connection error');
  }

  res.status(400).send({
    message: 'Something went wrong',
  });
};
```

Send a Postman request with a valid and an invalid email. Verify that the
console logs the appropriate message depending on the type of error encountered.
