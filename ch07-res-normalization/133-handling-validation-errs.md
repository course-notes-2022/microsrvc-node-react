# Handling Validation Errors

Let's now communicate the **validation results** to the client.

`signup.ts`:

```javascript
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';

const router = express.Router();

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors.array());
    }

    const { email, password } = req.body;
    res.status(200).json({});
  }
);

export { router as signupRouter };
```

Send a `POST` request via Postman to the `/api/users/signup` with any invalid
request body. You should receive a `400` response with output similar to the
following:

```json
[
  {
    "type": "field",
    "msg": "Email must be valid",
    "path": "email",
    "location": "body"
  },
  {
    "type": "field",
    "value": "",
    "msg": "Password must be between 4 and 20 characters",
    "path": "password",
    "location": "body"
  }
]
```

Our validation is working!
