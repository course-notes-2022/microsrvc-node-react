# Converting Errors to Responses

Our error-handling middleware is now aware of the **type** of errors it is
receiving. We now need to modify the **response** to return some
**error-specific output** to the user.

As a reminder, the entire reason we got into this discussion of error handling
is because we want to **standardize the error response structure** coming back
to the client from our **various** microservices, which may be written in
totally different frameworks or languages. Let's discuss now **what that
structure will be**.

## Our Error Response Structure

Our error response will be an **object** with an `errors` property. `errors` is
an **array of objects**. Each object will have a `message` property (String) and
an **optional** `field` property that is also a String:

```ts
interface ErrorResponse {
  errors: { message: string; field?: string }[];
}
```

`src/middleware/error-handler.ts`:

```ts
import { Request, Response, NextFunction } from 'express';
import { RequestValidationError } from '../errors/request-validation-err';
import { DatabaseConnectionError } from '../errors/database-connection-err';

export const errHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof RequestValidationError) {
    const formattedErrors = err.errors.map((e) => {
      if (e.type === 'field') {
        return { message: e.msg, field: e.path };
      }
    });
    return res.status(400).send({ errors: formattedErrors });
  }

  if (err instanceof DatabaseConnectionError) {
    return res.status(500).json({ errors: [{ message: err.reason }] });
  }

  // Unknown error type
  res.status(400).send({
    errors: [{ message: 'Unknown error' }],
  });
};
```

Test the new error-handling behavior in Postman, and verify that the returned
error object corresponds to the expected structure.
