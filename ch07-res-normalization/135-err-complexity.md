# Error Complexity

![error structure](./screenshots/err-structure.png)

The `AuthService` uses `express-validator` to validate requests and return error
response objects. **Other microservices in the app may be written in completely
different languages or frameworks, and return completely different response
structures**. Our frontend app has to expect **multiple different response
types**!

In a microservices environment, as we start to build out our different services,
we have to **ensure that each service sends a consistent error response
structure**!
