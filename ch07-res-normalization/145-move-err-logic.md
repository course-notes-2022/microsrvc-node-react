# Moving Logic into Errors

Right now, our `ErrorHandler` middleware contains the knowledge of how to
extract the information from _every error_. If the number of different types of
errors our application can create _increases_, `ErrorHandler` could quickly
become unmanageable.

We can fix this by adding a method to **each of our error subclasses**,
`serializeError()`, that takes all the error information and returns an array of
objects that conform to our **common error structure**, as well as the
appropriate HTTP status code:

![serialize error](./screenshots/serialize-error.png)

`ErrorHandler` will call `serializeError()` to obtain the information to return
to the caller.

## Refactor Error Classes with `serializeError()` Method

Make the following changes to `request-validation-err`:

`request-validation-err.ts`:

```ts
import { ValidationError } from 'express-validator';

export class RequestValidationError extends Error {
  statusCode = 400;

  constructor(public errors: ValidationError[]) {
    super();
    Object.setPrototypeOf(this, RequestValidationError.prototype);
  }

  serializeErrors() {
    return this.errors.map((e) => {
      if (e.type === 'field') {
        return { message: e.msg, field: e.path };
      }
    });
  }
}
```

Make the following changes to `database-connection-err`:

`database-connection-err.ts`:

```ts
export class DatabaseConnectionError extends Error {
  reason = 'Error connecting to database';
  statusCode = 500;
  constructor() {
    super();
    Object.setPrototypeOf(this, DatabaseConnectionError.prototype);
  }

  serializeErrors() {
    return [{ message: this.reason }];
  }
}
```

Make the following changes in `error-handler.ts`:

`src/middleware/error-handler.ts`:

```ts
import { Request, Response, NextFunction } from 'express';
import { RequestValidationError } from '../errors/request-validation-err';
import { DatabaseConnectionError } from '../errors/database-connection-err';

export const errHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err instanceof RequestValidationError) {
    return res.status(err.statusCode).send({ errors: err.serializeErrors() });
  }

  if (err instanceof DatabaseConnectionError) {
    return res.status(err.statusCode).json({ errors: err.serializeErrors() });
  }

  // Unknown error type
  res.status(400).send({
    errors: [{ message: 'Unknown error' }],
  });
};
```

Save and test in Postman.
