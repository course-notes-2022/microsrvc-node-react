# Communicating More Info to the Error Handler

Right now, we're getting the **same error message** regardless of what type of
error is thrown. We need to consume more information about the specific error in
our middleware. Refactor the `errorHandler` function as follows:

`src/middleware/error-handler.ts`:

```javascript
import { Request, Response, NextFunction } from 'express';

export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  console.log(err);
  res.status(400).send({
    // Return the specific error message
    message: err.message,
  });
};
```

Resend Postman requests: one with a **valid** email, and one with an **invalid**
email. Note that the response now contains a **specific message** from the
thrown error.
