# Solution for Error Handling

- **Issue 1**: We must have a consistently structured response from _all_
  servers, no matter what went wrong

- **Issue 2**: Many things can go wrong, not just validation of inputs to a
  request handler. Each of these needs to be handled consistently.

## Solutions

**Solution 1**: Write an error handling middleware to process errors, give them
a consistent structure, and send back to the browser.

**Solution 2**: Make sure we capture all possible errors using Express' error
handling mechanism (call the `next` function)

## Express Error Handling Refresher

For **sync** route handlers, if the code throws an error, **Express will catch
and process it**:

```javascript
app.get('/', (req, res) => {
  throw new Error('BROKEN'); // Express will catch this on its own.
});
```

For **async** route handlers, you must pass the error returned by the handler to
the `next()` function, which is the 3rd parameter passed to the route handler
function:

```javascript
app.get('/', (req, res, next) => {
  fs.readFile('/file-does-not-exist', (err, data) => {
    if (err) {
      next(err);
    } else {
      //...
    }
  });
});
```

### Writing Error Handlers in Express

Define error-handling middleware functions in the same was as other middleware
functions, except error-handling functions have four arguments instead of three:
(`err`, `req`, `res`, `next`):

```javascript
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something broke');
});
```
