# Async Error Handling

Observe the following code:

`index.ts`:

```javascript
// ...
import { NotFoundError } from './errors/not-found-error';

// ...
app.get('*', () => {
  throw new NotFoundError(); // Express will catch this error that occurs in a SYNCHRONOUS function
});
```

Error-handling middleware in Express works **differently** in **async
functions**:

`index.ts`:

```javascript
// ...
import { NotFoundError } from './errors/not-found-error';

// ...
app.get('*', async () => {
  throw new NotFoundError(); // This won't work!
});
```

If we have an async route handler (any handler with the `async` keyword, or that
has a promise or callback), we must pass `next()` as the third parameter to the
route handler function, and call `next()` with our error object:

```javascript
// ...
app.get('*', async (req, res, next) => {
  next(new NotFoundError());
});
```

## Workaround with `express-async-errors`

To avoid this issue, you can install the `express-async-errors` package. Using
it is very simple:

1. **Install** the `express-async-errors` package:
   `npm install express-async-errors --save`

2. **Require** the package before using it:

```javascript
// ...
import 'express-async-errors';
import { NotFoundError } from './errors/not-found-error';

// ...
app.get('*', async () => {
  throw new NotFoundError(); // This won't work!
});
```

**That's it**!
