# "`serializeErrors` not assignable to the same property in base type `CustomError`"

When adding the `serializeErrrors` function in the next lecture you will see the
following error:

```
[auth] src/errors/request-validation-error.ts(14,3): error TS2416: Property 'serializeErrors' in type 'RequestValidationError' is not assignable to the same property in base type 'CustomError'.
```

This is caused by the modifications we previously made in regard to
`express-validator` v7.

The function will need a small change to return an object with only the message:

```ts
serializeErrors() {
    return this.errors.map((err) => {
      if (err.type === 'field') {
        return { message: err.msg, field: err.path };
      }
      return { message: err.msg };
    });
  }
```
