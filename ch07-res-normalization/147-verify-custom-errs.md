# Verifying Our Custom Errors

Currently we are not checking to see that `serializeErrors()` returns an error
object with the correct, desired structure:

```ts
[{ message: string, field: string }];
```

It would be _really nice_ if we could have some **checks in place** to ensure
that when a new developer creates a new **custom error class**, that class has a
`statusCode` property and a `serializeErrors` method with the correct
implementation!

We have a couple of options:

1. Create a `CustomErrorInterface` interface, and ensure that
   `RequestValidationError` and `DatabaseConnectionError` return a
   `CustomErrorInterface` object with `statusCode` and `serializeErrors`
   properties:

```ts
interface CustomError {
  statusCode: number;
  serializeErrors(): {
    message: string;
    field?: string;
  };
}

//...

export class RequestValidationError extends Error implements CustomError {
  statusCode = 400;
  serializeErrors() {
    //...
  }
}
```

2. Create a new `CustomError` abstract class in Typescript:
   - Cannot be instantiated
   - Used to set up requirements for subclasses
   - Do create a Class when translated to JS, which means we can use it in
     `instanceof` checks!

**BOTH** options are completely valid; we'll go with option #2 for this course.
