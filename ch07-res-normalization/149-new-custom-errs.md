# Defining New Custom Errors

Create a new file in the `errors` directory, `not-found-error.ts`.

`/errors/not-found-error.ts`:

```javascript
import { CustomError } from './custom-error';

export class NotFoundError extends CustomError {
  statusCode = 404;

  constructor() {
    super('Route not found');

    Object.setPrototypeOf(this, NotFoundError.prototype);
  }

  serializeErrors() {
    return [{ message: 'Not Found' }];
  }
}
```

Implement a **route handler function** to handle requests to any routes we have
not explicitly defined in our application:

`index.ts`:

```javascript
// ...
import { NotFoundError } from './errors/not-found-error';

// ...
app.get('*', () => {
  throw new NotFoundError(); // Express will catch this and throw to our error-handling middleware
});
```

Note that this route handler works for only `GET` requests. We can make this
respond to all request types by changing `app.get()` to `app.all()`.
