# Building an Error-Handling Middleware

Create a new directory, `middleware`, inside your project:

```
|-auth
|--/src
|----/middleware
|------error-handler.ts
|----/routes
|------signin.ts
|------signout.ts
|------signup.ts
|------current-user.ts
|----/index.ts

```

`src/middleware/error-handler.ts`:

```javascript
import { Request, Response, NextFunction } from 'express';

export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // Send back a consistently-structured message
  console.log(err);
  res.status(400).send({
    message: 'Something went wrong',
  });
};
```

`index.ts`:

```javascript
import { errorHandler } from './middleware/error-handler.ts';

// ...

server.use(errorHandler);
```

`/route/signup.ts`:

```javascript
router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  (req: Request, res: Response) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      // Throw error from route handler to be caught by err handling middleware
      throw new Error('Invalid email or password');
    }

    const {
      body: { email, password },
    } = req;

    // Pretend the database is down 100% of the time
    console.log('attempting db connection...');
    throw new Error('database is always down');
    console.log('creating a user...');
    res.send({});
  }
);
```

Test the new error-handling behavior in Postman. Note that we get a `400`
request with the message `"Something broke"` for **both types of error**. This
is our **consistent error handling middleware in action**.
