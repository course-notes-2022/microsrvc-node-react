# Other Sources of Errors

Other scenarios in which we will want to return an error response:

1. Email is already in use
2. New user sign-up fails
3. Etc.

We need to capture the errors that occur **at every level** and construct and
send a consistent error response.
