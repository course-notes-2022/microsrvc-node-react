# Creating Route Handlers

We'll create the following routes for our `auth` service:

| Route                    | Method | Body                              | Purpose                        |
| ------------------------ | ------ | --------------------------------- | ------------------------------ |
| `/api/user/signup`       | POST   | {email: sring, password: string}  | Sign up for an account         |
| `/api/users/signin`      | POST   | {email: string, password: string} | Sign in to an existing account |
| `/api/users/signout`     | POST   | {}                                | Sign out                       |
| `/api/users/currentuser` | GET    | -                                 | Return info about the user     |

To standardize across apps, we're going to create **separate files** for our
route handlers for better organization. Create a new folder `routes` in the
`src` folder, and a **separate file** for each route handler in the table above.

In the `src/routes/current-user.ts` file, add the following:

```ts
import express from 'express';

const router = express.Router();

router.get('/api/users/currentuser', () => {});

export { router as currentUserRouter };
```

This will be the basic structure of all our route handlers.

Make the following changes in `index.ts`:

```ts
import express from 'express';
import { json } from 'body-parser';
import { currentUserRouter } from './routes/current-user';

const app = express();
const PORT = 3000;

app.use(json());

app.use(currentUserRouter);

app.listen(PORT, () => {
  console.log(`app listening on port ----> ${PORT}`);
});
```

Note that we `use` the `currentUserRouter` binding as we would any other
middleware.
