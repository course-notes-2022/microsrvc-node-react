# Scaffolding Routes

Let's modify the `currentUserRouter` as follows:

`current-user.ts`:

```javascript
import express from 'express';

const router = express.Router();

router.get('/api/users/currentuser', (req, res) => {
  res.status(200).json({ msg: 'Hi there!' });
});

export { router as currentUserRouter };
```

Replicate the above code in the `signin`, `signout`, and `signup` files per the
architecture diagram below:

![service architecture](./screenshots/services-architecture.png)
