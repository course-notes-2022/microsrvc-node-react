# Postman HTTPS Issues

Got an error when testing the route handler in the last video? If so, do the
following:

Open the Postman preferences. On MacOS, the hot key for this is CMD + ,

Find the setting called ‘SSL certificate verification’

Change this setting to ‘off’, as shown in the screenshot below:

![ssh cert verification off](./screenshots/ssh-off.png)
