# Subclassing for Custom Errors

Let's build out our custom error subclasses.

Create a new directory, `errors`, in the `src` directory:

```
|-auth
|--/src
|----/errors
|------request-validation-err.ts
|------database-connection-err.ts
|----/middleware
|------error-handler.ts
|----/routes
|------signin.ts
|------signout.ts
|------signup.ts
|------current-user.ts
|----/index.ts

```

`request-validation-err.ts`:

```javascript
import { ValidationError } from 'express-validator';

export class RequestValidationError extends Error {
    constructor(public errors: ValidationError[]) {
        super();

        // Only because we are exteding a built-in class
        Object.setPrototypeOf(this, RequestValidationError.prototype);
    }
}
```

`database-connection-err.ts`:

```javascript
export class DatabaseConnectionError extends Error {
  reason = 'Error connecting to database';
  constructor() {
    super();

    // Only because we are exteding a built-in class
    Object.setPrototypeOf(this, DatabaseConnectionError.prototype);
  }
}
```

`/route/signup.ts`:

```javascript
import { RequestValidationError } from '../errors/request-validation-err';
import { DatabaseConnectionError } from '../errors/database-connection-err';

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  (req: Request, res: Response) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      // Throw new RequestValidationError object to be caught by err handling middleware
      throw new RequestValidationError(errors.array);
    }

    const {
      body: { email, password },
    } = req;

    console.log('creating a user...');
    // Throw new DatabaseConnectionError
    throw new DatabaseConnectionError();

    res.send({});
  }
);
```

We have now refactored our route to throw our new custom errors. In the next
lesson, we'll refactor our **error handling function** to parse the errors and
return a more **specific error response**.
