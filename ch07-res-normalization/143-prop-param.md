# "Property `param` does not exist on type `AlternativeValidationError`"

In the upcoming lecture, we will be formatting our validation errors in
error-handler.ts.

The express-validator library recently released a breaking v7 version where the
ValidationError type is now a discriminated union:

[https://express-validator.github.io/docs/migration-v6-to-v7#telling-error-types-apart](https://express-validator.github.io/docs/migration-v6-to-v7#telling-error-types-apart)

As well as renaming `param` to `path`

[https://express-validator.github.io/docs/migration-v6-to-v7#renamed-properties](https://express-validator.github.io/docs/migration-v6-to-v7#renamed-properties)

So, we'll need to update our conditional to add a check for an error of type
field and use the new path property:

```js
if (err instanceof RequestValidationError) {
  const formattedErrors = err.errors.map((error) => {
    if (error.type === 'field') {
      return { message: error.msg, field: error.path };
    }
  });
  return res.status(400).send({ errors: formattedErrors });
}
```
