# Encoding More Information in an Error

We need to communicate more information about the error from the request handler
to the error-handling middleware.

We want an object like an `Error`, but we want to add in some custom properties.
Usually want to **subclass** something in this case!

![error subclasses](./screenshots/error-subclasses.png)

We will create `RequestValidationError` and `DatabseConnectionError` as
subclasses of `Error`. We will **throw** an instance of our new Error
subclasses; a `RequestValidationError` when request validation fails, and a
`DatabaseConnectionError` when the application fails to connect to the database.
Our error-handling middleware will contain logic to parse the type of error
subclass, and build an appropriate response to the user.
