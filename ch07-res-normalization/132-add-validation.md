# Adding Validation

We're going to use the
[`express-validator`](https://www.npmjs.com/package/express-validator) library
to validate the data coming in on our requests.

- `npm install --save express-validator`

Add the following to the `signup` router to validate the incoming request body
for an `email` and `password` field:

`signup.ts`:

```javascript
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';

const router = express.Router();

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors.array());
    }

    const { email, password } = req.body;
    res.status(200).json({});
  }
);

export { router as signupRouter };
```
