# Adding Password Hashing

Let's add hashing capability to our signup process. We'll create a **new class**
with the sole purpose of handling password hashing. Our class will:

1. Hash a password
2. Compare two password hashes for equality

This will help us keep our `UserModel` file a bit cleaner.

Create a new folder `services` in the `auth` directory. Create a new file
`password.ts`, and add the following:

```ts
import { scrypt, randomBytes } from 'crypto';
import { promisify } from 'util';

const scryptAsync = promisify(scrypt);

export class Password {
  static async toHash(password: string) {
    const salt = randomBytes(8).toString('hex');
    const buf = (await scryptAsync(password, salt, 64)) as Buffer;
    return `${buf.toString('hex')}.${salt}`;
  }

  static compare(storedPassword: string, suppliedPassword: string) {}
}
```

The `toHash` method takes care of hashing a password and returning the hash as a
string. We'll look at the `compare` method next.
