# Adding Static Properties to a Model

Here's a better way to write the `buildUser()` function so that it's a **part of
the model**, so that you can call it like the following:

```javascript
User.build({ email: 'test@test.com', password: 'foobarbaz' });
```

## How It's Done

`/src/models/users.ts`:

```javascript
import mongoose from 'mongoose';

interface UserAttrs {
  email: string;
  password: string;
}

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<any> {
  build(attrs: UserAttrs): any;
}

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

// Add the previous code for the `buildUser()` function
// to the schema object's `statics` property
userSchema.statics.build = (attrs: UserAttrs) => {
  return new User(attrs);
};

const User = mongoose.model<any, UserModel>('User', userSchema);


export { User };
```

Use the `build` function as follows:

```javascript
User.build({
  email: 'test@test.com',
  password: 'password',
});
```
