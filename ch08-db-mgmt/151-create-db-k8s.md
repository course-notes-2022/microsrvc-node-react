# Creating Databases in K8s

We will now begin to implement our `signup` flow. We will use `mongodb` to save
our users. Recall that every service will have **its own instance** of
`mongodb`. We will also use `mongoose`:

- `npm install mongoose --save`: Install mongoose package in `auth` application.

## Create Mongodb Deployment and Service

We will be running Mongodb inside a pod in our cluster. We will need a
`Deployment` file as well as a `ClusterIP` service to facilitate communication
with the Mongo pod **inside the cluster**.

`/infra/k8s/auth-mongo-depl.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth-mongo-depl
spec:
  replicas: 1
  selector:
    matchLabels:
      app: auth-mongo-depl
  template:
    metadata:
      labels:
        app: auth-mongo-depl
    spec:
      containers:
        - name: auth-mongo-depl
          image: mongo:latest
---
apiVersion: v1
kind: Service
metadata:
  name: auth-mongo-srvc
  type: ClusterIP
spec:
  selector:
    app: auth-mongo-depl
  ports:
    - name: db
      protocol: TCP
      port: 27017
      targetPort: 27017
```

## Deploy Mongodb Pod

- `kubectl apply -f auth-mongo-depl.yaml` OR
- `skaffold dev` if running Skaffold

**Note that right now**, if we delete or restart the pod running Mongodb, we
will lose all of the data in it! We will have a more in-depth discussion on this
later, _and fix it_.
