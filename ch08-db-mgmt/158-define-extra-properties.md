# Defining Extra Document Properties

In this section we'll look to solve Issue #2. Recall that issue #2 is related to
the fact that the set of properties we pass to the `User` constructor don't
necessarily match up with the properties Mongoose makes available on a `User`
object.

`/src/models/users.ts`:

```ts
import mongoose from 'mongoose';

// An interface that describes the properties
// required to create a new User
interface UserAttrs {
  email: string;
  password: string;
}

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAttrs): UserDoc;
}

// An interface that describes the properties
// that a User Document has
interface UserDoc extends mongoose.Document {
  email: string;
  password: string;
}

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

userSchema.statics.build = (attrs: UserAttrs) => {
  return new User(attrs);
};

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

const user = User.build({
  email: 'test@test.com',
  password: 'secret',
});

export { User };
```
