# Connecting to Mongo DB

We have created a new `Deployment` and `ClusterIP` service object for our
MongoDB instance. **Note however** that, for now, if we **delete or restart the
pod** with the `mongo` instance in it we will **lose all the data stored!**
We'll return to this later and find a fix.

For now, let's see how we can **connect to Mongo** from our `auth` application:

1. Install `mongoose` type definition file for Typescript:
   `npm install @types/mongoose`

2. Import and use `mongoose` in app server:

`index.ts`:

```javascript
import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import mongoose from 'mongoose'; // import mongoose

import { currentUserRouter } from './routes/current-user';
import { signinRouter } from './routes/signin';
import { signoutRouter } from './routes/signout';
import { signupRouter } from './routes/signup';
import { errorHandler } from './middlewares/error-handler';
import { NotFoundError } from './errors/not-found-error';

const app = express();
app.use(json());

app.use(currentUserRouter);
app.use(signinRouter);
app.use(signoutRouter);
app.use(signupRouter);

app.all('*', async (req, res) => {
  throw new NotFoundError();
});

app.use(errorHandler);

const start = async () => {
  // Connect to mongodb service running in the `auth-mongo-srvc` pod.
  // Note that the host name MUST be the service name!
  try {
    await mongoose.connect(
      'mongodb://auth-mongo-srv:27017/auth'
      // Use config object for mongoose versions < 6
      //   ,{
      //     useNewUrlParser: true,
      //     useUnifiedTopology: true,
      //     useCreateIndex: true,
      //   }
    );
    console.log('Connected to Mongodb');
  } catch (err) {
    console.log(err);
  }

  app.listen(3000, () => {
    console.log('Listening on port 3000!!!!!!!!');
  });
};

start();
```
