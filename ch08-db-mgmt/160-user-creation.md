# User Creation

Let's now get back to our signup process. Recall the sequence:

1. Client makes a request to `auth` service to register a new user

2. Check to see if user email address already exists in Mongodb `users`
   collection

3. If user does NOT exist, **hash incoming password**

4. Create a new user and save to DB

5. User is now considered logged-in. Send a cookie, JWT, something to indicate
   user is logged-in.

Refactor `signup.ts` as follows:

`auth/src/routes/signup.ts`:

```ts
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { RequestValidationError } from '../errors/request-validation-err';
import { User } from '../models/user';

const router = express.Router();

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw new RequestValidationError(errors.array());
    }

    const { email, password } = req.body;
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      console.log('email in use');
      return res.status(400).json({});
    } else {
      // temporarily skipping password hashing
      // create new user
      const user = User.build({ email, password });
      await user.save();

      return res.status(201).json(user);
    }
  }
);

export { router as signupRouter };
```

Send a new `POST` request to `http://ticketing.dev/api/users/signup` with a
valid email and password in the body. Verify that the output is similar to the
following:

```json
{
  "email": "foo@bar.com",
  "password": "secret",
  "_id": "6543c83aaae2927f7f6b0eae",
  "__v": 0
}
```
