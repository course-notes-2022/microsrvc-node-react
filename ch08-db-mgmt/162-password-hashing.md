# Note on Password Hashing

The four videos after this are all about storing a password securely with
MongoDB. Given that this is a course about microservices, I do not provide a
tremendous amount of information about the password hashing process. Instead, I
give a quick review of the process, and I assume you are already familiar with
it.

If you are not familiar with password hashing, or if you want to save some time,
I would skip over the next four videos. You will need to download the code
including password hashing and add it to your project if you skip these videos.

You can download this completed code by getting the zip file attached to the
lecture titled Section 8 Checkpoint (found at the end of this section). Unzip
the code, then drag and drop the auth directory into your project directory.
This will overwrite your code with the password hashing setup.
