# Mongoose Pre-Save Hooks

Let's now make use of our password hashing class in our `user` model. Make the
following change in `auth/src/models/user.ts`:

```ts
import mongoose from 'mongoose';
import { Password } from '../services/password';

// An interface that describes the properties
// required to create a new User
interface UserAttrs {
  email: string;
  password: string;
}

// An interface that describes the properties
// that a User Model has
interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAttrs): UserDoc;
}

// An interface that describes the properties
// that a User Document has
interface UserDoc extends mongoose.Document {
  email: string;
  password: string;
}

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

userSchema.statics.build = (attrs: UserAttrs) => {
  return new User(attrs);
};

// NOTE: MUST use the 'function' keyword
// NOT an arrow function here
userSchema.pre('save', async function (done) {
  if (this.isModified('password')) {
    const hashed = await Password.toHash(this.get('password'));
    this.set('password', hashed);
  }
  done();
});

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

const user = User.build({
  email: 'test@test.com',
  password: 'secret',
});

export { User };
```

Test creating a new user again in Postman. Send a `POST` request to
`http://ticketing.dev/api/users/signup` with a valid email and password. Note
that the status code returned is `201` and the output is similar to the
following (with the hashed password returned):

```json
{
  "email": "foo@baz.com",
  "password": "5912aae999245d5a8ec69c9271e87fce9b13d6bfb002736402d866c01702eac9882a1aea18cd90848d399d65fbcccd7d5805c96f6d7721e438192ca6fcb8cd2a.bd7aca6c8cb01d04",
  "_id": "6543f2323562e5eefbe4cf03",
  "__v": 0
}
```
