# Understanding the Signup Flow

Let's look at what we'll need to do in our `signup` flow:

![signup flow](./screenshots/signup-flow.png)

1. Client makes a request to `auth` service to register a new user

2. Check to see if user email address already exists in Mongodb `users`
   collection

3. If user does NOT exist, **hash incoming password**

4. Create a new user and save to DB

5. User is now considered logged-in. Send a cookie, JWT, something to indicate
   user is logged-in.

We'll create a Mongoose `User` model to help us. We'll need to create a little
bit of configuration to get Mongoose and Typescript to work togeter.
