# Type Checking `User` Properties

Let's now look at how to solve the problem of getting Typescript and Mongoose to
work together. Specifically, we want to:

> Ensure that Typescript can perform **type checking** on the arguments that we
> pass to the `new User()` constructor.

`/src/models/users.ts`:

```javascript
import mongoose from 'mongoose';

// Interface describes the properties required
// to create a new User
interface UserAttrs {
  email: string;
  password: string;
}

const userSchema = new mongoose.Schema({
  email: {
    type: String, // Note that these types have NOTHING to do with TS!
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

const User = mongoose.model('User', userSchema);

// We will call the `buildUser()` function
// INSTEAD
// of `new User()`
// EVERY TIME we want to create a new User object!
const buildUser = (attrs: UserAttrs) => {
  // TS is now aware of the types and arguments that must be passed to create a User!
  return new User(attrs);
};

export { User, buildUser };
```

By calling `buildUser` **instead** of `new User()`, we are **getting Typescript
involved** in creating new `User` objects. If we ever provide an incorrect
property to the `attrs` object, **Typescript will alert us**.
