# Proper Error Handling

If an existing user attempts to signup **again** with the same email, we want to
handle it in the **same way we handled the previous custom errors**:

```ts
if (existingUser) {
  throw new BadRequestError('email in use');
}
```

Let's do that now. In the `errors` directory,create a new file
`bad-request-error.ts`:

```ts
import { CustomError } from './custom-error';

export class BadRequestError extends CustomError {
  statusCode = 400;

  constructor(public message: string) {
    super(message);

    Object.setPrototypeOf(this, BadRequestError.prototype);
  }
  serializeErrors(): { message: string }[] {
    return [{ message: this.message }];
  }
}
```

Update the `signup.ts` file to use the new error class:

```ts
import express, { Request, Response } from 'express';
import { body, validationResult } from 'express-validator';
import { RequestValidationError } from '../errors/request-validation-err';
import { User } from '../models/user';
import { BadRequestError } from '../errors/bad-request-error';

const router = express.Router();

router.post(
  '/api/users/signup',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      throw new RequestValidationError(errors.array());
    }

    const { email, password } = req.body;
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      // Use the new BadRequestError class
      throw new BadRequestError('email in use');
    } else {
      const user = User.build({ email, password });
      await user.save();

      return res.status(201).json(user);
    }
  }
);

export { router as signupRouter };
```
