# Creating the User Model

Create a new file in `/src/models/users.ts`:

```javascript
import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  email: {
    type: String, // Note that these types have NOTHING to do with TS!
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
});

// Create the model from the schema
const User = mongoose.model('User', userSchema);

export { User };
```

## TS Issues: Create a New User

Right now, attempting to do the following:

```javascript
new User({
  email: 'test@test.com',
  badPropertyNameHere: 'foobarbaz',
});
```

Will not throw **any** errors in TS! TS currently has **no understanding** of
what the structure of a `User` object **should be**. How can we **teach** TS
about the `User`, and what its structure should be?
