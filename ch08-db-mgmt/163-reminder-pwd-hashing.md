# Reminder on Password Hashing

Let's work on hashing our user's password before saving to the database.

## BAD Approach

**NEVER, EVER store passwords as plain text in a database!** This is a BAD
security practice, and exposes us and our users' sensitive information to
breaches!

## What is Hashing a Password?

**Hashing** runs our user's password through an algorithm that produces a
**unique** string from the original password, a.k.a. the "hash". A good hashing
algorithm produces the **same output every time** when given the **same input**.
Therefore, only someone who knows the **original password** (theoretically this
should be the **user him/herself**) can access our application. Furthermore, the
**original password cannot be reverse-engineered from the hash**.

We store the **hash** in our database. When a user attempts to sign in, we take
the password they send and hash it with the **exact same hash function**. If the
**hashes** match, the user is correctly authenticated.
