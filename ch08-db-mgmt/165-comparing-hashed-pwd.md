# Comparing Hashed Passwords

Let's now implement the `compare` method. Note that the hash we store in the
database is actually a **concatenation** of the hashed password and the `salt`
we generated in the `toHash` method, separated by a `.` To compare for equality,
we must hash the **supplied password** with the **same stored salt** and compare
the result with the full **stored hash**:

```ts
import { scrypt, randomBytes } from 'crypto';
import { promisify } from 'util';

const scryptAsync = promisify(scrypt);

export class Password {
  static async toHash(password: string) {
    const salt = randomBytes(8).toString('hex');
    const buf = (await scryptAsync(password, salt, 64)) as Buffer;
    return `${buf.toString('hex')}.${salt}`;
  }

  static async compare(storedPassword: string, suppliedPassword: string) {
    const [hashedPassword, salt] = storedPassword.split('.');
    const buf = (await scryptAsync(suppliedPassword, salt, 64)) as Buffer;
    return buf.toString('hex') === hashedPassword;
  }
}
```
