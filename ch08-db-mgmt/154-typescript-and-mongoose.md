# Getting Typescript and Mongoose to Cooperate

Unfortunately, there are a few big issues that complicate using Typescript
together with `mongoose`. We'll look at two of them in this lesson.

## Terminology Refresher

1. **collection**: similar to a **table** in a relational DB. Stores individual
   records in MongoDB.

2. **Mongoose "Users" Model**: a class that represents the entire `Users`
   collection. We use this class to run queries against the `Users` collection.

3. **Mongoose "User" Document**: a class that represents a **single user**.

![key terminology](./screenshots/terms.png)

## Issue #1: Creating a New User Document

Consider creating the following new document in the `Users` collection:

```javascript
new User({
  email: 'test@test.com', // Typescript wants to make sure we are providing the correct DATA TYPES; Mongoose does not give TS the necessary information!
  password: 'alfour932084',
});
```

## Issue #2: Mongoose-added Properties

The properties that we pass to the `User` constructor when creating the document
**may not** match up with the properties available on the `User`, as
**`mongoose` can add additional properties** that the Typescript interface/class
is not expecting!
