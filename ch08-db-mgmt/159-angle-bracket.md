# What Are Those Angle Brackets For?

````javascript

interface UserModel extends mongoose.Model<UserDoc> {
  build(attrs: UserAttrs): UserDoc;
}

/*
The "<>" syntax is TS Generic syntax.
When we call a function in JS or TS, we normally
pass arguments that customize how that function
behaves. Generics are like arguments to the function
`model`, but instead of values, they are TYPES.

CMD+click or CTRL+click will take you to the docs
for `mongoose.model`. There you will see somethig similar to the following:

// ```
export function model<T extends Document>(name: string, schema?:Schema, collection?: string, skipInit?: boolean): Model<T>;

export function model<T extends Document, U extends Model<T>>(name: string, schema?:Schema, collection?: string, skipInit?: boolean): U;
// ```

Notice the second declaration. The "T" inside the <>
corresponds to the FIRST argument when the function is called. The "U" corresponds to the SECOND argument. In our case, the `mongoose.model` function
will return a UserModel type object.
*/
const User = mongoose.model<UserDoc, UserModel>('User', userSchema);
````
