# Lessons from App #1

We've learned a lot about microservices, Docker, K8s, and Skaffold. Let's now
take that knowledge and build our first big app. Before we get started, let's
review some lessons-learned:

## Lessons Learned

1. **The big challenge in microservices is DATA**.

2. There is **more than one way** to share data between services. We will focus
   on **async** communication, as it is more difficult.

3. Async communication focuses on communicating changes in our data using
   **events sent to an event bus**.

4. Async communication encourages each service to be **self-sufficient**.
   Relatively easy to handle temporary downtime or new service createion.

5. Docker makes it easier to package up services.

6. K8s is a pain to set up, but makes it easy to deploy and scale services.

## Pain Points from App #1

1. **Lots of duplicated code**!

2. Really hard to picture the flow of events between services

3. Really hard to remember what properties an event should have

4. Really hard to test some event flows

5. My machine is getting slow running K8s and everything else

6. Is there a series of events which, if emitted in a certain order, would break
   our data model? E.g., a user created a post _and_ a comment _simultaneously_?

## Solutions!

| Problem                                                                                        | Solution                                                                             |
| ---------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------ |
| Lots of duplicated code                                                                        | Build a central library as an `npm` module to share code betw our different projects |
| Really hard to picture the flow of events between services                                     | Precisely define all our events in this shared library                               |
| Really hard to remember what properties an event should have                                   | Write _everything_ in Typescript                                                     |
| Really hard to test some event flows                                                           | Write tests for as much as possible/reasonable                                       |
| My machine is getting slow running K8s and everything else                                     | Run a K8s cluster in the cloud and develop on it _almost_ as quickly as local        |
| Is there a series of events which, if emitted in a certain order, would break our application? | Introduce a lot of code to handle concurrency issues                                 |
