# Note on Code Reloading

If you did not see your server restart after changing the index.ts file, do the
following:

1. Open the package.json file in the ‘auth’ directory

2. Find the ‘start’ script

Update the start script to the following:

> `ts-node-dev --poll src/index.ts`
