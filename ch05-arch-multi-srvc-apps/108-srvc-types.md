# Services

1. `auth`: Everything related to user signin /sign up/ sign out

2. `tickets`: Ticket creation/editing. Knows whether a ticket can be updated

3. `orders`: Order creation/editing

4. `expiration`: Watches for orders to be created, cancels them after 15 minutes

5. `payments`: Handles credit card payments. Cancels orders if payment fails,
   completes if payment succeeds.

**Note that we are creating a separate service to manage each type of
resource**. This is not necessarily the _best_ approach for every case; depends
on your use case.
