# Ingress-Nginx Setup

Let's now start development of our `auth` service. First, let's ensure that we
can send/receive requests to the service manually with a client like Postman.

Create a "dummy" route as follows. We'll then send a request and attempt to hit
the route in the browser:

`index.ts`:

```javascript
import express from 'express';
import { json } from 'body-parser';

const server = express();

const PORT = 3000;

server.use(json());

// Add GET route for /api/users/currentuser to test
server.get('/api/users/currentuser', (req, res) => {
  res.status(200).json({ msg: 'Hi there' });
});

server.listen(PORT, () => {
  console.log(`SERVER listening on port ${PORT}`);
});
```

We now want to **access our running server** in the cluster (from **outside**
the cluster). To do so, recall that we need to do either of the following:

1. Set up a `NodePort` service

2. Set up an `IngressService` to create routing rules for `nginx` that routes
   incoming requests to the appropriate microservice

**Note**: If you did **not** delete your `ingress-nginx` cluster that was
previously running from the last exercise, _you only need to write a new config
file for ingress-nginx and load it into the cluster_. If you did delete the
cluster,
[run the `minikube` command in the documentation](https://kubernetes.github.io/ingress-nginx/deploy/#minikube).

Create the config file as follows:

`infra/k8s/ingress-srvr.yaml`:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-srvc
  annotations:
    nginx.ingress.kubernetes.io/use-regex: 'true' # Informs nginx to expect paths including regular expressions
spec:
  ingressClassName: nginx
  rules:
    - host: ticketing.dev # for LOCAL development
      http:
        paths:
          - path: /api/users/?(.*)
            pathType: ImplementationSpecific
            backend:
              service:
                name: auth-ms-internal
                port:
                  number: 3000
```

\*\*Add the `infra/k8s/ingress-srv.yaml` file to the `manifests.rawYaml`
property in your `skaffold.yaml` file:

```yaml
apiVersion: skaffold/v4beta7
kind: Config
metadata:
  name: ticketing-ms
build:
  local:
    concurrency: 1
    tryImportMissing: false
    useDockerCLI: false
    push: false
  artifacts:
    - image: jfarrow02/auth-ms
      context: auth
      docker:
        dockerfile: Dockerfile
manifests:
  rawYaml:
    - infra/k8s/auth-ms-depl.yaml
    - infra/k8s/ingress-srv.yaml # Add here

```
