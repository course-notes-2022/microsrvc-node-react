# `auth` K8s Setup

Let's go ahead and set up K8s support for our `auth` service right away:

1. Create a `Dockerfile` inside the `auth` directory with the following content:

```Dockerfile
FROM node:20-alpine
RUN mkdir -p /app
WORKDIR /app
COPY src ./src
COPY package.json .
RUN npm install
CMD ["npm", "run", "start"]
```

2. Build the image with the `docker buildx` command:

> `docker buildx build -t <image-name>:<tag-name> -f <Dockerfile-location> .`

3. As a sanity check, run a container from the image to ensure that it works.

4. Create a new directory in the project root, `infra/k8s`.

5. Create a K8s manifest for the `auth-ms` Deployment and ClusterIP service
   objects:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth-depl
spec:
  replicas: 1
  selector:
    matchLabels: # Tells deployment HOW TO FIND the pods it creates
      app: auth-depl
  template: # How to create a POD for this DEPL
    metadata:
      labels:
        app: auth-depl # Create a POD with THIS LABEL
    spec:
      containers:
        - name: auth
          image: jfarrow02/auth-ms

---
apiVersion: v1
kind: Service
metadata:
  name: auth-ms-internal
spec:
  type: ClusterIP
  selector: # How to find the PODS for this serivce to govern
    app: auth-depl
  ports:
    - name: auth
      protocol: TCP
      port: 3000
      targetPort: 3000
```
