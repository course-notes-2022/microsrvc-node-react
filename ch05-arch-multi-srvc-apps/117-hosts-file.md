# Hosts File and Security Warning

Recall that we must update our OS' `host` file in order to direct requests to
`ticketing.dev` to our local machine.

For Linux machines, you must use the output of `minikube ip` command as host:

`/etc.hosts` (Linux)

```sh
# ...

# 127.0.0.1 ticketing.dev # MacOS
000.000.000.000 ticketing.dev # Linux

```

In the browser address bar, enter `http://ticketing.dev/api/users/currentuser`.
Note that you will see a security warning similar to the following:

![security warning](./screenshots/security-warning.png)

By default, `ingress-nginx` attempts to use the `HTTPS` protocol, with a
_self-signed certificate_. By default, Chrome _does not allow_ self-signed
certificates. This won't be a problem for our users once we deploy. For now, we
can get around this by clicking **anywhere in the browser window and typing**:

> this is unsafe

**_OR_**: Just send the request via Postman (or a similar client)!
