# Resource Types

What are the different resource (data) types we'll need?

**User**:

| Name        | Type   |
| ----------- | ------ |
| `email`     | string |
| `password ` | sring  |

**Ticket**:

| Name      | Type         |
| --------- | ------------ |
| `title`   | string       |
| `price`   | number       |
| `userId`  | ref to User  |
| `orderId` | ref to Order |

**Order**(represents customer intent/attempt to purchase ticket):

| Name        | Type                                             |
| ----------- | ------------------------------------------------ |
| `userId`    | ref to User                                      |
| `status`    | `Created\|Cancelled\|AwaitingPayment\|Completed` |
| `ticketId`  | ref to Ticket                                    |
| `expiresAt` | Date                                             |

**Charge**:

| Name             | Type                         |
| ---------------- | ---------------------------- |
| `orderId`        | ref to Order                 |
| `status`         | `Created\|Failed\|Completed` |
| `amount`         | number                       |
| `stripeId`       | string                       |
| `stripeRefundId` | string                       |
