# Adding `skaffold`

Let's set up our `skaffold` config file. This file will watch our `infra`
directory and:

- Automatically apply changes to a config file to the K8s cluster
- Sync any changes inside the `auth` directory with the appropriate running
  container in our cluster

1. Create a `skaffold.yaml` file in the project root:

```yaml
apiVersion: skaffold/v4beta7
kind: Config
metadata:
  name: ticketing-ms--
build:
  local:
    concurrency: 1
    tryImportMissing: false
    useDockerCLI: false
    push: false
  artifacts:
    - image: jfarrow02/auth-ms
      context: auth
      docker:
        dockerfile: Dockerfile
manifests:
  rawYaml:
    - infra/k8s/auth-ms-depl.yaml
```

**Note**: run `skaffold init` to create this file automatically. Select the
`Docker` options when prompted.

2. Stop `skaffold` if currently running from a previous project with `CTRL + C`.

3. Run `skaffold dev`. Skaffold will start watching all source file dependencies
   for all artifacts specified in the project. As changes are made to these
   source files, Skaffold will rebuild and retest the associated artifacts, and
   redeploy the new changes to your cluster.

_Note: The video specifies a previous version of the `skaffold.yaml` file, as
follows:_

```yaml
apiVersion: skaffold/v2alpha3
kind: Config
deploy:
  kubectl:
    manifests:
      - ./infra/k8s/*
build:
  local:
    push: false # do not push image to Dockerhub by default
  artifacts: # Artifacts to build
    - image: jfarrow02/auth-ms
      context: auth # Folder that contains code for this image
      docker:
        dockerfile: Dockerfile
      sync: # Tell skaffold what to do when files inside `auth` dir change
        manual:
          - src: 'src/**/*.ts' # Watch all files in `src` directory with a `.ts` extension
            dest: . #
```

_**According to the most recent version of the API documentation**, the
corresponding configuration seems to be the described in step 1._
