# Events Architecture

| Event            |
| ---------------- |
| `UserCreated`    |
| `UserUpdated`    |
| `OrderCreated`   |
| `OrderCancelled` |
| `OrderExpired`   |
| `TicketCreated`  |
| `TicketUpdated`  |
| `ChargeCreated`  |

## Architecture Diagram

![architecture diagram](./screenshots/ticket_app_event_arch.png)

`common` will be an `npm` module we'll create for common code shared between
module.

`expiration` will make use of Redis for data store.

Each service will use its **own MongoDB** instance.

We'll use the `NATS Streaming Server` (not to be confused with `N`etwork
`A`ddress `T`ranslation), an **event bus implementation** similar to what we
built in the previous application, but more fully-featured.
