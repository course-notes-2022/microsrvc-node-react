# A Note on Typescript

As a reminder, we are going to be using Typescript for the rest of this course.

It is OK if you don’t know Typescript! I put together a series of videos on the
basics of Typescript for you. They will teach you everything you need to know
about Typescript.

You can find these videos at the end of the course in a section called ‘Appendix
B - Basics of Typescript’. If you don’t know Typescript, please watch these
videos before continuing this section.
